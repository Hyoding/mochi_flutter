import 'package:mochi_flutter/core/model/Coordinate.dart';

class OrderSequence {
  int sequence;
	String orderKey;
	Coordinate coordinate;

  OrderSequence({this.sequence, this.orderKey, this.coordinate});

  factory OrderSequence.fromJson(Map<String, dynamic> json) {
    return OrderSequence(
      sequence: json['sequence'],
      orderKey: json['orderKey'],
      coordinate: Coordinate.fromJson(json['coordinate']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'sequence': sequence,
      'orderKey': orderKey,
      'coordinate': coordinate.toJson(),
    };
  }
}