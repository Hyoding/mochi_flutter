import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:badges/badges.dart';
import 'package:map_launcher/map_launcher.dart';
import 'package:mochi_flutter/config/AppState.dart';
import 'package:mochi_flutter/config/Constants.dart';
import 'package:mochi_flutter/core/MapBuilder.dart';
import 'package:mochi_flutter/core/model/Address.dart';
import 'package:mochi_flutter/core/model/Coordinate.dart';
import 'package:mochi_flutter/orders/Order.dart';
import 'package:mochi_flutter/orders/OrdersScreen.dart';
import 'package:mochi_flutter/trips/Trip.dart';
import 'package:mochi_flutter/util/HttpUtil.dart';
import 'package:mochi_flutter/util/UiUtil.dart';
import 'package:latlong/latlong.dart';
// ignore: implementation_imports
import 'package:map_launcher/src/models.dart' as co;
import 'package:mochi_flutter/util/Util.dart';
import 'package:reorderables/reorderables.dart';
import 'package:expandable/expandable.dart';

import 'LocationService.dart';

class NavigationWidget extends StatefulWidget {
  final Trip trip;
  NavigationWidget({@required this.trip});
  @override
  _NavigationWidgetState createState() => _NavigationWidgetState();
}

class _NavigationWidgetState extends State<NavigationWidget> {
  Timer timer;
  List<TripOrderWidget> orderSequence;
  bool reorder = false;
  @override
  void setState(fn) {
    if(mounted) {
      super.setState(fn);
    }
  }
  @override
  void initState() {
    super.initState();
    initialize();
    LocationService.getInstance().startLocationService(widget.trip);
  }
  @override
  void dispose() {
    super.dispose();
    timer?.cancel();
  }

  Future<void> initialize() async {
    if (!mounted) return;
    timer = Timer.periodic(Duration(seconds: 10), (_) {
      setState(() { });
    });
  }

  @override
  Widget build(BuildContext context) {
    Trip trip = widget.trip;
    orderSequence = trip.orderSequence.map((os) {
      int idx = trip.orderSequence.indexOf(os);
      return TripOrderWidget(
        setParentState: setState,
        number: idx + 1,
        order: trip.orders.firstWhere((o) => o.key == os.orderKey),
      );
    }).toList();
    ScrollController _scrollController = PrimaryScrollController.of(context) ?? ScrollController();
    return Column(
      children: [
        Flexible(
          // flex: 2,
          child: FutureBuilder(
            future: buildWayPoint(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Center(child: CircularProgressIndicator());
              } else {
                return embedMap(snapshot.data);
              }
            },
          ),
        ),
        Flexible(
          // flex: 3,
          child: Container(
            decoration: !reorder ? null : BoxDecoration(
              border: Border.all(
                color: Colors.orange,
                width: 3,
              ),
            ),
            child: Column(
              children: [
                Container(
                  height: 50,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(width: 1),
                      Flexible(
                        child: widget.trip.distance == 0 ? Text('Estimate not calculated for this plan') 
                        : Tooltip(
                          message: 'This is an estimate from initial optimized route',
                          child: Text('Distance ${(widget.trip.distance).toStringAsFixed(1)} km'),
                        ),
                      ),
                      widget.trip.distance == 0 ? Text('') 
                      : Tooltip(
                        message: 'This is an estimate from initial optimized route',
                        child: Text('Duration ${(widget.trip.duration).toStringAsFixed(0)} min'),
                      ),
                      FlatButton(
                        onPressed: () {
                          setState(() {
                            reorder = !reorder;
                          });
                        },
                        child: Icon(
                          reorder ? Icons.done : Icons.list,
                          color: Colors.orange,
                        ),
                      ),
                    ],
                  ),
                ),
                Flexible(
                  child: CustomScrollView(
                    controller: _scrollController,
                    slivers: [
                      ReorderableSliverList(
                        enabled: reorder,
                        delegate: ReorderableSliverChildListDelegate(orderSequence),
                        onReorder: _onReorder,
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Future buildWayPoint() async {
    Trip trip = widget.trip;
    List<Coordinate> waypoints = List();
    var myLoc = await Util.getMyCoordinate();
    waypoints.add(myLoc);
    for (int i = 0; i < trip.orderSequence.length; ++i) {
      var orderSeq = trip.orderSequence[i];
      waypoints.add(orderSeq.coordinate);
    }
    waypoints.add(myLoc);
    return waypoints;
  }

  Widget embedMap(List coordinates) {
    Trip trip = widget.trip;
    List<Marker> markers = coordinates.map((coord) {
      var index = coordinates.indexOf(coord);
      bool isCanceledOrDelivered;
      Order order;
      if (index > 0) {
        order = trip.orders.firstWhere((o) => o.address.coordinate.latitude == coord.latitude && o.address.coordinate.longitude == coord.longitude);
        isCanceledOrDelivered = order.status != 'Out for delivery';
      }
      return Marker(
        width: 50.0,
        height: 50.0,
        point: LatLng(coord.latitude, coord.longitude),
        builder: (ctx) => Container(
          child: GestureDetector(
            onTap: () {
              if (index > 0) {
                openOrderDialog(
                  context: ctx,
                  selectedOrder: order,
                  viewOnly: true,
                );
              }
            },
            child: Badge(
              toAnimate: false,
              badgeColor: index == 0 ? Colors.blue : isCanceledOrDelivered ? Colors.grey : getOrderColour(order) == null ? Colors.black : getOrderColour(order),
              position: BadgePosition.topEnd(top: 12, end: 15),
              badgeContent: Text(index == 0 ? ' ' : (index).toString(),
                style: TextStyle(
                  color: Colors.white
                ),
              ),
              child: Text('')
            ),
          ),
        ),
      );
    }).toList();
    return FlutterMap(
      options: MapOptions(	
        center: LatLng(trip.location.latitude, trip.location.longitude),
        zoom: 13.0,
      ),
      layers: [
        TileLayerOptions(
          urlTemplate: AppState.getInstance().mapboxStyle,
          additionalOptions: {
            'accessToken': AppState.getInstance().mapboxKey,
            'id': 'mapbox.streets',
          },
        ),
        MarkerLayerOptions(
          markers: markers,
        ),
      ],
    );
  }

  void _onReorder(int oldIndex, int newIndex) async {
    Trip trip = widget.trip;
    List orderSequence = trip.orderSequence;
    var row = orderSequence.removeAt(oldIndex);
    orderSequence.insert(newIndex, row);
    await HttpUtil.postConveniently(
      url: Url.UPDATE_TRIP_SEQUENCE,
      data: trip.toJson(),
      cb: (response) {
        dynamic tripRes = response.responseData;
        Trip updatedTrip = Trip.fromJson(tripRes);
        setState(() {
          trip = updatedTrip;
        });
      },
    );
  }

}

class TripOrderWidget extends StatefulWidget {
  final Function setParentState;
  final int number;
  final Order order;
  TripOrderWidget({this.number, this.order, this.setParentState});
  @override
  _TripOrderWidgetState createState() => _TripOrderWidgetState();
}

class _TripOrderWidgetState extends State<TripOrderWidget> {
  @override
  Widget build(BuildContext context) {
    return ExpandablePanel(
      header: header(),
      expanded: expanded(),
    );
  }

  Widget header() {
    Color textColour;
    bool isCanceled = false;
    bool isDeliveredOrCanceled = (widget.order.status == 'Delivered' || widget.order.status == 'Canceled' || widget.order.status == 'Rejected') ? true : false;
    if (isDeliveredOrCanceled) {
      textColour = Colors.grey;
      if (widget.order.status == 'Canceled' || widget.order.status == 'Rejected') {
        isCanceled = true;
      }
    } else {
      textColour = getOrderColour(widget.order);
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          child: FlatButton(
            onPressed: () {
              openOrderDialog(
                context: context,
                selectedOrder: widget.order,
                viewOnly: true,
              );
            },
            child: Text(
              '${widget.number}.  ${isCanceled ? "Canceled" : shortAddress(widget.order.address)}',
              style: TextStyle(
                color: textColour,
                decoration: isCanceled ? TextDecoration.lineThrough : null,
              ),
            ),
          ),
        ),
        Text(
          Util.formatShortDate(widget.order.deliverBy),
          style: TextStyle(
            color: textColour,
            decoration: isCanceled ? TextDecoration.lineThrough : null
          ),
        ),
      ],
    );
  }

  Widget expanded() {
    bool isDeliveredOrCanceled = (widget.order.status == 'Delivered' || widget.order.status == 'Canceled' || widget.order.status == 'Rejected') ? true : false;
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        OutlinedButton.icon(
          label: Text('Start'),
          onPressed: isDeliveredOrCanceled ? null : navigate,
          icon: Icon(
            Icons.navigation,
            color: Colors.blue,
          ),
          style: ButtonStyle(),
        ),
        SizedBox(width: 20),
        OutlinedButton.icon(
          label: Text('Done'),
          onPressed: isDeliveredOrCanceled ? null : markOrderDelivered,
          icon: Icon(
            Icons.done,
            color: Colors.blue,
          ),
        ),
      ],
    );
  }

  void navigate() async {
    final coords = co.Coords(
      widget.order.address.coordinate.latitude,
      widget.order.address.coordinate.longitude,
    );
    final title = widget.order.address.streetAddress;
    final availableMaps = await MapLauncher.installedMaps;
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return SafeArea(
          child: SingleChildScrollView(
            child: Container(
              child: Wrap(
                children: <Widget>[
                  for (var map in availableMaps)
                    ListTile(
                      onTap: () => {
                        // TODO: update order to coming to you?
                        map.showMarker(
                          coords: coords,
                          title: title,
                        ),
                      }, 
                      title: Text(map.mapName),
                      leading: Icon(Icons.map),
                    ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  void markOrderDelivered() {
    UiUtil.showAlert(
      title: 'Completed order?',
      content: Text("You won't be able to undo this action"),
      onConfirm: () async {
        MapBuilder mb = HttpUtil.createMap();
        mb.addEntry('key', widget.order.key);
        Coordinate coordinate = await Util.getMyCoordinate();
        Address address = Address(coordinate: coordinate);
        mb.addEntry('address', address.toJson());
        await HttpUtil.postConveniently(
          url: Url.DELIVERED_ORDER,
          data: mb.map,
        );
        widget.order.status = 'Delivered';
        widget.setParentState(() {});
      }
    );
  }

}
