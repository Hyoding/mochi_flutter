import 'dart:isolate';

import 'dart:ui';

import 'package:background_locator/background_locator.dart';
import 'package:background_locator/location_dto.dart';
import 'package:background_locator/settings/android_settings.dart';
import 'package:background_locator/settings/ios_settings.dart';
import 'package:background_locator/settings/locator_settings.dart';
import 'package:mochi_flutter/config/AppState.dart';
import 'package:mochi_flutter/config/Constants.dart';
import 'package:mochi_flutter/core/MapBuilder.dart';
import 'package:mochi_flutter/core/model/Coordinate.dart';
import 'package:mochi_flutter/util/HttpUtil.dart';
import 'package:mochi_flutter/util/Util.dart';

import 'Trip.dart';

class LocationService {
  static const String _isolateName = "LocatorIsolate";
  static ReceivePort port;

  Trip _trip;
  Coordinate _lastLocation;

  static LocationService _instance;

  LocationService._();

  static getInstance() {
    if (_instance == null) {
      _instance = LocationService._();
    }
    return _instance;
  }

  void init() async {
    print('init loc service');
    port = ReceivePort();
    IsolateNameServer.registerPortWithName(port.sendPort, _isolateName);
    port.listen((dynamic data) {
      print('listen' + data.toString());
      _lastLocation = Coordinate(
        latitude: data.latitude,
        longitude: data.longitude,
      );
      updateLocation(_lastLocation);
    });
    _lastLocation = await Util.getMyCoordinate();
    initPlatformState();
  }

  static void dispose() {
    print('dispose loc service');
    port.close();
    IsolateNameServer.removePortNameMapping(_isolateName);
    BackgroundLocator.unRegisterLocationUpdate();
  }

  Future<void> initPlatformState() async {
    await BackgroundLocator.initialize();
  }

  static void callback(LocationDto locationDto) async {
    final SendPort send = IsolateNameServer.lookupPortByName(_isolateName);
    send?.send(locationDto);
  }

  void startLocationService(Trip trip) {
    AppState appState = AppState.getInstance();
    _trip = trip;
    BackgroundLocator.registerLocationUpdate(callback,
      autoStop: false,
      disposeCallback: dispose,
      iosSettings: IOSSettings(
        accuracy: LocationAccuracy.NAVIGATION,
        distanceFilter: appState.updateLocationDistance,
      ),
      androidSettings: AndroidSettings(
        accuracy: LocationAccuracy.NAVIGATION,
        interval: appState.updateLocationFrequency,
        distanceFilter: appState.updateLocationDistance,
      )
    );
  }

  void updateLocation(Coordinate location) async {
    print('update loc');
    MapBuilder mb = HttpUtil.createMap();
    mb.addEntry('key', _trip.key);
    mb.addEntry("location", location.toJson());
    HttpUtil.postConveniently(
      url: Url.UPDATE_TRIP_LOCATION,
      data: mb.map,
      showAlertOnSuccess: false,
      showAlertOnFail: false,
      showProgressIndicator: false,
    );
  }

}