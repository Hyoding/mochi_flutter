import 'package:flutter/material.dart';
import 'package:mochi_flutter/config/AppState.dart';
import 'package:mochi_flutter/config/Constants.dart';
import 'package:mochi_flutter/core/MapBuilder.dart';
import 'package:mochi_flutter/core/components/Drawer.dart';
import 'package:mochi_flutter/core/model/Coordinate.dart';
import 'package:mochi_flutter/login/AppConfigLoader.dart';
import 'package:mochi_flutter/manage/tag/Tag.dart';
import 'package:mochi_flutter/orders/Order.dart';
import 'package:mochi_flutter/orders/OrdersScreen.dart';
import 'package:mochi_flutter/trips/Trip.dart';
import 'package:mochi_flutter/util/HttpUtil.dart';
import 'package:mochi_flutter/util/UiUtil.dart';
import 'package:mochi_flutter/util/Util.dart';

import 'LocationService.dart';
import 'NavigationWidget.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

const String FINISH_TRIP = 'Finish';

class MyTripScreen extends StatelessWidget {
  static const String PATH = '/myTrip';
  static const String TITLE = 'My Trip';
  @override
  Widget build(BuildContext context) {
    return RootTripWidget();
  }
}

class RootTripWidget extends StatefulWidget {
  @override
  _RootTripWidgetState createState() => _RootTripWidgetState();
}

class _RootTripWidgetState extends State<RootTripWidget> {
  Future tripInfoFuture;
  Trip myTrip;
  @override
  void initState() {
    super.initState();
    tripInfoFuture = getMyTrip();
    AppConfigLoader.reloadAppData();
    LocationService.getInstance().init();
  }
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: tripInfoFuture,
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Center(child: CircularProgressIndicator());
        } else {
          Widget child;
          if (myTrip != null) {
            child = NavigationWidget(trip: myTrip);
          } else {
            child = PickOrdersWidget();
          }
          return Scaffold(
            drawer: MyDrawer(),
            appBar: AppBar(
              title: Text(MyTripScreen.TITLE),
              centerTitle: true,
              actions: myTrip == null ? null : [
                PopupMenuButton<String>(
                  onSelected: handleClick,
                  itemBuilder: (BuildContext context) {
                    return {FINISH_TRIP}.map((String choice) {
                      return PopupMenuItem<String>(
                        value: choice,
                        child: Text(choice),
                      );
                    }).toList();
                  },
                ),
              ],
            ),
            body: child,
          );
        }
      }
    );
  }

  void handleClick(String value) {
    String content = '';
    String url = '';
    switch (value) {
      case FINISH_TRIP:
        content = 'Orders that are not marked delivered will be put back into Active queue';
        url = Url.FINISH_TRIP;
        break;
    }

    UiUtil.showAlert(
      title: 'Are you sure',
      content: Text(content),
      onConfirm: () async {
        MapBuilder mb = HttpUtil.createMap();
        mb.addEntry('key', myTrip.key);
        await HttpUtil.postConveniently(
          url: url,
          data: mb.map,
        );
        Navigator.pushNamedAndRemoveUntil(context, MyTripScreen.PATH, (r) => false);
      },
    );
  }

  void refresh() {
    setState(() { });
  }

  Future getMyTrip() async {
    final String eKey = AppState.getInstance().employee.key;
    await HttpUtil.getConveniently(
      url: Url.FIND_CURRENT_TRIP,
      queryParameters: HttpUtil.formParams('employeeKey', eKey),
      cb: (myResponse) {
        dynamic tripRes = myResponse.responseData;
        print('retrieved trip');
        if (tripRes == null) {
          return false;
        }
        myTrip = Trip.fromJson(tripRes);
      },
    );
    return true;
  }
}

class PickOrdersWidget extends StatefulWidget {
  @override
  _PickOrdersWidgetState createState() => _PickOrdersWidgetState();
}

class _PickOrdersWidgetState extends State<PickOrdersWidget> {
  Future getOrdersFuture;
  Map<Order, bool> orders;
  bool shdSelectAll = false;
  Map<Tag, bool> tagSelections;
  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async{
    getOrdersFuture = fetchOrdersToDeliver();
    setState(() {
    });
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    print('onloading');
    if(mounted)
    setState(() {
    });
    _refreshController.loadComplete();
  }

  @override
  void initState() {
    super.initState();
    getOrdersFuture = fetchOrdersToDeliver();
    initFilters();
  }

  @override
  void dispose() {
    super.dispose();
    getOrdersFuture = null;
  }

  void initFilters() async {
    tagSelections = Map();
    AppState appState = AppState.getInstance();
    appState.tags.forEach((tag) {
      tagSelections.putIfAbsent(tag, () => false);
    });
  }

  bool isTagSelected() {
    bool isTagSelected = false;
    tagSelections.forEach((key, value) {
      if (value) {
        isTagSelected = true;
        return;
      }
    });
    return isTagSelected;
  }

  void selectAll(bool selected) {
    getVisibleOrders().forEach((order, value) { 
      orders.update(order, (value) => selected);
    });
    setState(() {
      shdSelectAll = selected;
    });
  }

  Map<Order, bool> getVisibleOrders() {
    Map<Order, bool> ordersToShow = Map();
    if (isTagSelected()) {
      // AND filter
      orders.forEach((o, value) {
        bool meetsAllCriteria = true;
        // get selected tags
        List selectedTags = List();
        tagSelections.forEach((tag, isSelected) {
          if (isSelected) {
            selectedTags.add(tag);
          }
        });

        // check if all tags are present in the order if yes then add
        selectedTags.forEach((tag) {
          final idx = o.tagsKey.indexWhere((tk) => tk == tag.key);
          if (idx == -1) {
            meetsAllCriteria = false;
            return;
          }
        });
        if (meetsAllCriteria) {
          ordersToShow.putIfAbsent(o, () => value);
        }
      });

      // OR filter
      // tagSelections.forEach((tag, isSelected) {
      //   if (isSelected) {
      //     orders.forEach((o, value) {
      //       final idx = o.tagsKey.indexWhere((tk) => tk == tag.key);
      //       if (idx >= 0) {
      //         ordersToShow.putIfAbsent(o, () => value);
      //       }
      //     });
      //   }
      // });

    } else {
      ordersToShow = orders;
    }
    return ordersToShow;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getOrdersFuture,
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Center(child: CircularProgressIndicator());
        } else {
          print('this needs to show after refresh');
          Map<Order, bool> ordersToShow = getVisibleOrders();
          return Column(
            children: [
              Container(
                padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
                child: CheckboxListTile(
                  title: Text('Selected: ${countSelected()}'),
                  value: shdSelectAll,
                  onChanged: (newValue) => selectAll(newValue),
                  secondary: IconButton(
                    onPressed: () {
                      showOrderTagsDialog(
                        context: context,
                        tagSelections: tagSelections,
                        setParentState: setState,
                        onChanged: () {
                          setState(() {});
                        },
                      );
                    },
                    icon: Icon(isTagSelected() ? Icons.filter_alt : Icons.filter_alt_outlined),
                  ),
                ),
              ),
              Flexible(
                flex: 6,
                child: SmartRefresher(
                  enablePullDown: true,
                  controller: _refreshController,
                  onRefresh: _onRefresh,
                  onLoading: _onLoading,
                  child: ListView.separated(
                    itemBuilder: (context, index) {
                      Order order = getOrder(ordersToShow, index);
                      bool isSelected = ordersToShow[order];
                      return CheckboxListTile(
                        title: PackageOrderWidget(
                          order: order,
                        ),
                        value: isSelected,
                        onChanged: (newValue) {
                          setState(() {
                            orders.update(order, (value) => newValue);
                          });
                        },
                      );
                    },
                    separatorBuilder: (_,__) => Divider(thickness: 1),
                    itemCount: ordersToShow.length
                  ),
                ),
              ),
              Flexible(
                child: Container(
                  height: double.infinity,
                  width: double.infinity,
                  child: OutlinedButton(
                    onPressed: () {
                      int count = countSelected();
                      final int maxOrdersInTrip = AppState.getInstance().tripMaxNumbOrders;
                      if (count == 0) {
                        UiUtil.showAlert(content: Text("No order was selected"));
                      } else if (count > maxOrdersInTrip) {
                        UiUtil.showAlert(content: Text("You can only select up to $maxOrdersInTrip"));
                      } else {
                        createTrip();
                      }
                    },
                    child: Text('START TRIP'),
                  ),
                ),
              ),
            ],
          );
        }
      },
    );
  }

  Order getOrder(Map<Order, bool> ordersToShow, int idx) {
    int i = 0;
    Order order;
    ordersToShow.forEach((key, value) { 
      if (i == idx) {
        order = key;
      }
      i++;
    });
    return order;
  }

  void createTrip() async {
    Coordinate location = await Util.getMyCoordinate();
    List<String> selected = getSelectedOrders().map((e) => e.key).toList();
    MapBuilder mb = HttpUtil.createMap();
    mb.addEntry('orderKeys', selected);
    mb.addEntry('location', location.toJson());
    await HttpUtil.postConveniently(
      url: Url.START_TRIP,
      data: mb.map,
    );
    Navigator.pushNamedAndRemoveUntil(context, MyTripScreen.PATH, (r) => false);
  }

  int countSelected() {
    int selected = 0;
    orders.forEach((key, val) {
      if (val) {
        selected++;
      }
    });
    return selected;
  }

  List<Order> getSelectedOrders() {
    List<Order> selectedOrders = List();
    orders.forEach((order, isSelected) {
      if (isSelected) {
        selectedOrders.add(order);
      }
    });
    return selectedOrders;
  }

  Future fetchOrdersToDeliver() async {
    final String bKey = AppState.getInstance().business.key;
    await HttpUtil.getConveniently(
      url: Url.GET_ORDERS_TO_DELIVER,
      queryParameters: HttpUtil.formParams('businessKey', bKey),
      cb: (myResponse) {
        List ordersResponse = myResponse.responseData;
        orders = Map();
        ordersResponse.forEach((element) {
          orders.putIfAbsent(Order.fromJson(element), () => false);
        });
        print('retrieved orders');
      },
    );
    return orders;
  }
}

class PackageOrderWidget extends StatefulWidget {
  final Order order;
  PackageOrderWidget({this.order});

  @override
  _PackageOrderWidgetState createState() => _PackageOrderWidgetState();
}

class _PackageOrderWidgetState extends State<PackageOrderWidget> {
  bool isSelected = false;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          flex: 4,
          child: Text(
            shortAddress(widget.order.address) + ", " + widget.order.address.city,
            style: TextStyle(color: getOrderColour(widget.order))
          )
        ),
        Flexible(
          flex: 2,
          child: Text(
            Util.formatShortDate(widget.order.deliverBy),
            style: TextStyle(color: getOrderColour(widget.order))
          ),
        ),
      ],
    );
  }
}