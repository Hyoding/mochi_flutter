import 'package:mochi_flutter/core/model/Coordinate.dart';
import 'package:mochi_flutter/orders/Order.dart';

import 'OrdeSequence.dart';

class Trip {
  String key;
  String employeeKey;
  String employeeInitial;
  String employeePhone;
  List<String> orderKeys;
  DateTime tripEndTime;
  Coordinate location;
  double duration;
  double distance;
  List<OrderSequence> orderSequence;
  List<Order> orders;

  Trip({
    this.key,
    this.employeeKey,
    this.employeeInitial,
    this.employeePhone,
    this.orderKeys,
    this.tripEndTime,
    this.location,
    this.duration,
    this.distance,
    this.orderSequence,
    this.orders,
  });

  factory Trip.fromJson(Map<String, dynamic> json) {
    final List orderSequenceList = json['orderSequence'];
    final List orderList = json['orders'];
    return Trip(
      key: json['key'],
      employeeKey: json['employeeKey'],
      employeeInitial: json['employeeInitial'],
      employeePhone: json['employeePhone'],
      orderSequence: orderSequenceList.map((os) => OrderSequence.fromJson(os)).toList(),
      tripEndTime: json['tripEndTime'] == null ? null : DateTime.parse(json['tripEndTime']).toLocal(),
      location: Coordinate.fromJson(json['location']),
      duration: json['duration'],
      distance: json['distance'],
      orders: orderList.map((o) => Order.fromJson(o)).toList(),
    );
  }

  Map<String, dynamic> toJson() => {
    'key': key,
    'employeeKey': employeeKey,
    'orderKeys': orderKeys,
    'tripEndTime': tripEndTime == null ? null : tripEndTime.toUtc().toIso8601String(),
    // 'location': location,
    'duration': duration,
    'distance': distance,
    'orderSequence': orderSequence,
  };

}