import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:mochi_flutter/config/AppState.dart';
import 'package:mochi_flutter/config/Constants.dart';
import 'package:mochi_flutter/core/MapBuilder.dart';
import 'package:mochi_flutter/manage/subscription/SubscriptionPlans.dart';
import 'package:mochi_flutter/util/HttpUtil.dart';
import 'package:mochi_flutter/util/UiUtil.dart';
import 'package:url_launcher/url_launcher.dart';

class ChangeSubscriptionScreen extends StatelessWidget {
  static const String PATH = '/changeSubscription';
  static const String TITLE = 'Change Subscription';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(TITLE),
      ),
      body: ChangeSubscriptionWidget(),
      resizeToAvoidBottomPadding: false
    );
  }
}

class ChangeSubscriptionWidget extends StatefulWidget {
  @override
  _ChangeSubscriptionWidgetState createState() => _ChangeSubscriptionWidgetState();
}

class _ChangeSubscriptionWidgetState extends State<ChangeSubscriptionWidget> {
  SubscriptionPlans plans;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: fetch(),
      builder: (_, snapshot) {
        if (!snapshot.hasData) {
          return Center(child: CircularProgressIndicator());
        } else {
          var subscriptions = plans.subscriptionList.map((s) {
            bool isCurrentPlan = s.name == plans.mySubscription.name;
            return SubscriptionPlanWidget(
              isCurrentPlan: isCurrentPlan,
              subscriptionConfig: s,
              selectCb: chooseNewSubscription,
            );
          }).toList();
          AppState appState = AppState.getInstance();
          return SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.all(5),
              child: Column(
                children: [
                  OutlinedButton(
                    onPressed: () {
                      launch('${appState.urlWeb}');
                    },
                    child: Text('Go to our website for plan details'),
                  ),
                  Column(
                    children: subscriptions,
                  ),
                ],
              ),
            ),
          );
        }
      },
    );
  }

  Future fetch() async {
    final String bKey = AppState.getInstance().business.key;
    await HttpUtil.getConveniently(
      url: Url.GET_SUBSCRIPTION_PLANS,
      queryParameters: HttpUtil.formParams('businessKey', bKey),
      cb: (myResponse) {
        plans = SubscriptionPlans.fromJson(myResponse.responseData);
      },
    );
    return plans;
  }

  void chooseNewSubscription(SubscriptionConfig subscription) async {
    if (subscription.name == plans.mySubscription.name) {
      return;
    }
    UiUtil.showAlert(
      title: 'Please confirm',
      content: Text('Are you sure about this change'),
      onConfirm: () async {
        MapBuilder mb = HttpUtil.createMap();
        mb.merge(subscription.toJson());
        print('selected a new plan ${subscription.name}');
        await HttpUtil.postConveniently(
          url: Url.CHANGE_SUBSCRIPTION_PLAN,
          data: mb.map,
        );
        setState(() {});
      },
    );
  }

}

class SubscriptionPlanWidget extends StatelessWidget {
  final bool isCurrentPlan;
  final SubscriptionConfig subscriptionConfig;
  final Function selectCb;
  SubscriptionPlanWidget({this.isCurrentPlan, this.subscriptionConfig, this.selectCb});
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topCenter,
      child: Card(
        child: InkWell(
          splashColor: Colors.blue,
          onTap: () {
            selectCb(subscriptionConfig);
          },
          child: Container(
            decoration: !isCurrentPlan ? null : BoxDecoration(
              border: Border.all(
                width: 3,
                color: Colors.blue,
              ),
            ),
            padding: EdgeInsets.all(20),
            width: double.infinity,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  subscriptionConfig.name,
                  textScaleFactor: 2,
                  style: !isCurrentPlan ? null : TextStyle(
                    color: Colors.blue,
                  ),
                ),
                SizedBox(height: 20),
                Text(
                  '\$${subscriptionConfig.price.toStringAsFixed(2)}',
                  textScaleFactor: 1.5,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 20),
                isCurrentPlan ? Text(
                  'CURRENT PLAN',
                  style: TextStyle(
                    color: Colors.blue,
                  ),
                ) : Text(
                  'SELECT PLAN'
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}