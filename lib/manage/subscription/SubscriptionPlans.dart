class SubscriptionConfig {
  
  String name;
  double price;

  SubscriptionConfig({this.name, this.price});

  factory SubscriptionConfig.fromJson(Map<String, dynamic> json) {
    return SubscriptionConfig(
      name: json['name'],
      price: json['price'],
    );
  }

  Map<String, dynamic> toJson() => {
    'name': name,
    'price': price,
  };

}

class SubscriptionPlans {

  SubscriptionConfig mySubscription;
  List<SubscriptionConfig> subscriptionList;

  SubscriptionPlans({this.mySubscription, this.subscriptionList});

  factory SubscriptionPlans.fromJson(Map<String, dynamic> json) {
    List subscriptions = json['subscriptionList'];
    return SubscriptionPlans(
      mySubscription: SubscriptionConfig.fromJson(json['mySubscription']),
      subscriptionList: subscriptions.map((s) => SubscriptionConfig.fromJson(s)).toList(),
    );
  }

}