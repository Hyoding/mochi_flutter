import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mochi_flutter/config/AppState.dart';
import 'package:mochi_flutter/core/components/BaseDrawerScreen.dart';
import 'package:mochi_flutter/manage/report/GenerateReportScreen.dart';
import 'package:mochi_flutter/manage/subscription/ChangeSubscriptionScreen.dart';
import 'package:mochi_flutter/manage/employee/ManageEmployeeScreen.dart';
import 'package:mochi_flutter/manage/tag/ManageTagScreen.dart';
import 'package:url_launcher/url_launcher.dart';

class ManageScreen extends BaseDrawerScreen {
  static const String PATH = '/manage';
  static const String TITLE = 'Manage';
  @override
  Widget getBody(BuildContext context) {
    return ManageWidget();
  }

  @override
  String getTitle() {
    return TITLE;
  }
}

class ManageWidget extends StatefulWidget {
  @override
  _ManageWidgetState createState() => _ManageWidgetState();
}

class _ManageWidgetState extends State<ManageWidget> {
  @override
  Widget build(BuildContext context) {
    var appState = AppState.getInstance();
    return SingleChildScrollView(
      child: Column(
        children: [
          ManageItem(
            icon: Icon(Icons.people),
            title: 'Employees',
            description: 'Manage your business better by adding more employees',
            onClick: () => Navigator.pushNamed(context, ManageEmployeeScreen.PATH),
          ),
          ManageItem(
            icon: Icon(Icons.tag),
            title: 'Tags',
            description: 'Tag your orders for filtering',
            onClick: () => Navigator.pushNamed(context, ManageTagScreen.PATH),
          ),
          ManageItem(
            icon: Icon(Icons.upload_file),
            title: 'Import orders',
            description: 'Import orders in bulk using excel files',
            onClick: () => launch('${appState.urlWebImportOrders}/${appState.business.key}'),
          ),
          ManageItem(
            icon: Icon(Icons.analytics),
            title: 'Reports',
            description: 'Generate reports created with your own data',
            onClick: () => Navigator.pushNamed(context, GenerateReportScreen.PATH),
          ),
          Visibility(
            visible: appState.employee.type == 'Owner',
            child: ManageItem(
              icon: Icon(Icons.attach_money),
              title: 'Subscription plan',
              description: 'Choose the right subscription plan for your business',
              onClick: () => Get.to(ChangeSubscriptionScreen()),
            ),
          ),
        ],
      ),
    );
  }
}

class ManageItem extends StatelessWidget {
  final Icon icon;
  final String title;
  final String description;
  final Function onClick;
  ManageItem({
    @required this.icon,
    @required this.title,
    @required this.description,
    @required this.onClick
  });
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        FlatButton(
          onPressed: () {
            onClick();
          },
          child: Container(
            padding: EdgeInsets.all(10),
            child: Row(
              children: [
                icon,
                SizedBox(width: 20),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(title, textScaleFactor: 1.5,),
                      SizedBox(height: 5),
                      Text(description, style: TextStyle(fontWeight: FontWeight.w300)),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        Divider(thickness: 1),
      ],
    );
  }
}
