class Employee {
  
  String key;
	String firstName;
	String lastName;
	String phoneNumber;
	String email;
  String type;

  Employee({this.key, this.firstName, this.lastName, this.phoneNumber, this.email, this.type});

  factory Employee.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      return null;
    }
    return Employee(
      key: json['key'],
      firstName: json['firstName'],
      lastName: json['lastName'],
      phoneNumber: json['phoneNumber'],
      email: json['email'],
      type: json['type'],
    );
  }

  Map<String, dynamic> toJson() => {
    'key': key,
    'firstName': firstName?.trim(),
    'lastName': lastName?.trim(),
    'phoneNumber': phoneNumber,
    'email': email,
    'type': type,
  };

  @override
  String toString() {
    return firstName + ' ' + lastName;
  }

}