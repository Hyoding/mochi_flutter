import 'package:flutter/material.dart';
import 'package:mochi_flutter/config/AppState.dart';
import 'package:mochi_flutter/config/Constants.dart';
import 'package:mochi_flutter/core/MapBuilder.dart';
import 'package:mochi_flutter/manage/employee/AddEditEmployeeScreen.dart';
import 'package:mochi_flutter/manage/employee/Employee.dart';
import 'package:mochi_flutter/util/HttpUtil.dart';
import 'package:mochi_flutter/util/UiUtil.dart';

class ManageEmployeeScreen extends StatefulWidget {
  static const String PATH = '/manageEmployees';
  static const String TITLE = 'Manage Employees';
  @override
  _ManageEmployeeScreenState createState() => _ManageEmployeeScreenState();
}

class _ManageEmployeeScreenState extends State<ManageEmployeeScreen> {
  List<Employee> employees = List();
  Future fetch() async {
    final String bKey = AppState.getInstance().business.key;
    await HttpUtil.getConveniently(
      url: Url.GET_EMPLOYEES,
      queryParameters: HttpUtil.formParams('businessKey', bKey),
      cb: (response) {
        List employeesMap = response.responseData;
        employees = employeesMap.map((i) => Employee.fromJson(i)).toList();
      }
    );
    return true;
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(ManageEmployeeScreen.TITLE),
      ),
      body: FutureBuilder(
        future: fetch(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          } else {
            return Container(
              padding: EdgeInsets.all(10),
              child: Column(
                children: [
                  Container(
                    width: double.infinity,
                    child: OutlinedButton(
                      onPressed: () => Navigator.pushNamed(context, AddEditEmployeeScreen.PATH, arguments: {
                        'viewOnly': false,
                        'employee': Employee(),
                      }),
                      child: Text('Add'),
                    ),
                  ),
                  SizedBox(height: 20),
                  Flexible(
                    child: Column(
                      children: [          
                        Flexible(
                          child: ListView.builder(
                            itemBuilder: (context, index) {
                              return EmployeeWidget(
                                employee: employees[index],
                                removeEmployee: removeEmployee,
                              );
                            },
                            itemCount: employees.length
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            );
          }
        }
      ),
    );
  }
  void removeEmployee(Employee deletedEmp) async {
    MapBuilder mb = HttpUtil.createMap();
    mb.merge(deletedEmp.toJson());
    await HttpUtil.postConveniently(
      url: Url.REMOVE_EMPLOYEE,
      data: mb.map,
      cb: (_) {
        setState(() {});
      }
    );
  }

}

class ManageEmployeesWidget extends StatefulWidget {
  final Function reload;
  final List<Employee> employees;
  ManageEmployeesWidget({@required this.employees, this.reload});

  @override
  _ManageEmployeesWidgetState createState() => _ManageEmployeesWidgetState(reload: reload);
}

class _ManageEmployeesWidgetState extends State<ManageEmployeesWidget> {
  final Function reload;
  _ManageEmployeesWidgetState({this.reload});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Employees',
              style: TextStyle(fontSize: 16),
            ),
            OutlinedButton(
              onPressed: () => Navigator.pushNamed(context, AddEditEmployeeScreen.PATH, arguments: {
                'viewOnly': false,
                'employee': Employee(),
              }),
              child: Text('Add'),
            ),
          ],
        ),
        SizedBox(height: 20),
        Flexible(
          child: Column(
            children: [          
              Flexible(
                child: ListView.builder(
                  itemBuilder: (context, index) {
                    return EmployeeWidget(
                      employee: widget.employees[index],
                      removeEmployee: removeEmployee,
                    );
                  },
                  itemCount: widget.employees.length
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  void removeEmployee(Employee deletedEmp) async {
    MapBuilder mb = HttpUtil.createMap();
    mb.merge(deletedEmp.toJson());
    await HttpUtil.postConveniently(
      url: Url.REMOVE_EMPLOYEE,
      data: mb.map,
      cb: (_) {
        reload();
      }
    );
  }

}

class EmployeeWidget extends StatelessWidget {
  final Employee employee;
  final Function(Employee employee) removeEmployee;
  EmployeeWidget({this.employee, this.removeEmployee});
  @override
  Widget build(BuildContext context) {
    bool isOwner = employee.type == 'Owner';
    return FlatButton(
      onPressed: () {
        Navigator.pushNamed(context, AddEditEmployeeScreen.PATH, arguments: {
          'viewOnly': true,
          'employee': employee,
        });
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
            flex: 3,
            child: Container(
              width: double.infinity,
              child: Text('${employee.firstName} ${employee.lastName}'),
            ),
          ),
          Flexible(
            child: FittedBox(
              fit: BoxFit.fitWidth,
              child: Text(employee.type),
            ),
          ),
          Visibility(
            visible: isOwner,
            child: SizedBox(width: 75),
          ),
          Visibility(
            visible: !isOwner,
            child: Container(
              width: 75,
              child: DropdownButtonHideUnderline(
                child: DropdownButton(
                  icon: Icon(Icons.menu),
                  onChanged: (val) {
                    if (val == 'edit') {
                      Navigator.pushNamed(context, AddEditEmployeeScreen.PATH, arguments: {
                        'viewOnly': false,
                        'employee': employee,
                      });
                    } else {
                      UiUtil.showAlert(
                        title: 'Remove employee?',
                        content: Text("You won't be able to undo this action"),
                        onConfirm: () => removeEmployee(employee),
                      );
                    }
                  },
                  items: [
                    DropdownMenuItem(
                      value: 'edit',
                      child: Text('edit'),
                    ),
                    DropdownMenuItem(
                      value: 'delete',
                      child: Text('delete'),
                    ),
                  ],
                )
              ),
            ),
          ),
        ],
      ),
    );
  }
}