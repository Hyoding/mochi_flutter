import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mochi_flutter/config/Constants.dart';
import 'package:mochi_flutter/core/MapBuilder.dart';
import 'package:mochi_flutter/manage/employee/ManageEmployeeScreen.dart';
import 'package:mochi_flutter/util/HttpUtil.dart';
import 'package:mochi_flutter/util/UiUtil.dart';
import 'package:mochi_flutter/util/Util.dart';

import 'Employee.dart';
import '../ManageScreen.dart';

class AddEditEmployeeScreen extends StatefulWidget {
  static const String PATH = '/addEditEmployee';
  final bool viewOnly;
  final Employee employee;
  AddEditEmployeeScreen({this.viewOnly, this.employee});
  @override
  _AddEditEmployeeScreenState createState() => _AddEditEmployeeScreenState(
    viewOnly: viewOnly,
    employee: employee,
  );
}

class _AddEditEmployeeScreenState extends State<AddEditEmployeeScreen> {
  final bool viewOnly;
  Employee employee;

  final _formKey = GlobalKey<FormState>();
  final TextEditingController tcFirstName = TextEditingController();
  final TextEditingController tcLastName = TextEditingController();
  final TextEditingController tcPhone = TextEditingController();
  final TextEditingController tcEmail = TextEditingController();
  final TextEditingController tcRole = TextEditingController();

  @override
  void initState() {
    super.initState();
    // clone
    Map empMap = employee.toJson();
    employee = Employee.fromJson(empMap);
  }

  _AddEditEmployeeScreenState({this.viewOnly, this.employee});
  @override
  Widget build(BuildContext context) {
    // TODO: get this from appState
    List<String> employeeRoles = ['Manager', 'Operator', 'Driver'];
    final double fieldsPadding = 5;
    String title;
    if (employee.key == null) {
      title = 'Add Employee';
    } else {
      if (viewOnly) {
        title = 'View Employee';
      } else {
        title = 'Edit Employee';
      }
    }
    tcFirstName.text = employee.firstName;
    tcLastName.text = employee.lastName;
    tcPhone.text = employee.phoneNumber;
    tcEmail.text = employee.email;
    tcRole.text = employee.type;
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text(title),
      ),
      body: Container(
        padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              SizedBox(height: fieldsPadding),
              TextFormField(
                readOnly: viewOnly,
                validator: validateField,
                controller: tcFirstName,
                onChanged: (val) => employee.firstName = val,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'First Name',
                ),
              ),
              SizedBox(height: fieldsPadding),
              TextFormField(
                readOnly: viewOnly,
                validator: validateField,
                controller: tcLastName,
                onChanged: (val) => employee.lastName = val,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Last Name',
                ),
              ),
              SizedBox(height: fieldsPadding),
              TextFormField(
                onTap: () {
                  if (viewOnly && tcPhone.text.length > 0) {
                    UiUtil.showCallOrTextAlert(tcPhone.text);
                  }
                },
                style: TextStyle(
                  color: viewOnly ? Colors.blue : null,
                ),
                readOnly: viewOnly,
                controller: tcPhone,
                keyboardType: TextInputType.phone,
                onChanged: (val) => employee.phoneNumber = val,
                validator: validatePhone,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Phone',
                ),
              ),
              SizedBox(height: fieldsPadding),
              TextFormField(
                readOnly: employee.key != null,
                controller: tcEmail,
                onChanged: (val) => employee.email = val,
                validator: validateEmail,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Email',
                ),
              ),
              SizedBox(height: fieldsPadding),
              TextFormField(
                validator: validateField,
                controller: tcRole,
                readOnly: true,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Role',
                ),
                onTap: viewOnly ? null : () {
                  UiUtil.showAlert(
                    title: 'Pick',
                    content: Column(
                      children: employeeRoles.map((role) => OutlinedButton(
                        onPressed: () {
                          setState(() {
                            employee.type = role;
                          });
                          Get.back();
                        },
                        child: Text(role),
                      )).toList(),
                    ),
                  );
                },
              ),
              Visibility(
                visible: !viewOnly,
                child: Container(
                  width: double.infinity,
                  child: OutlinedButton(
                    onPressed: () async {
                      if (!UiUtil.isFormReady(_formKey)) {
                        return;
                      }
                      if (employee.key == null) {
                        addNewEmployee();
                      } else {
                        editEmployee();
                      }
                    },
                    child: Text('Save'),
                  ),
                ),
              ),
            ],
          ),
        ),
      )
    );
  }

  void addNewEmployee() async {
    MapBuilder mb = HttpUtil.createMap();
    mb.merge(employee.toJson());
    await HttpUtil.postConveniently(
      url: Url.ADD_EMPLOYEE,
      data: mb.map,
      cb: (_) {
        Navigator.pop(context);
        Navigator.popAndPushNamed(context, ManageEmployeeScreen.PATH);
      },
    );
  }

  void editEmployee() async {
    MapBuilder mb = HttpUtil.createMap();
    mb.merge(employee.toJson());
    await HttpUtil.postConveniently(
      url: Url.UPDATE_EMPLOYEE,
      data: mb.map,
      cb: (_) {
        Navigator.pop(context);
        Navigator.popAndPushNamed(context, ManageEmployeeScreen.PATH);
      },
    );
  }
  
  String validatePhone(String phone) {
    if (Util.isPhoneNumber(phone)) {
      return null;
    }
    return 'Invalid';
  }

  String validateEmail(String email) {
    if (Util.isEmail(email)) {
      return null;
    }
    return 'Invalid';
  }

  String validateField(String priceStr) {
    if (priceStr == null || priceStr.isEmpty) {
      return "Cannot be empty";
    }
    return null;
  }
}