import 'package:flutter/material.dart';
import 'package:mochi_flutter/config/AppState.dart';
import 'package:mochi_flutter/config/Constants.dart';
import 'package:mochi_flutter/core/MapBuilder.dart';
import 'package:mochi_flutter/manage/tag/AddEditTagScreen.dart';
import 'package:mochi_flutter/util/HttpUtil.dart';
import 'package:mochi_flutter/util/UiUtil.dart';

import 'Tag.dart';

class ManageTagScreen extends StatefulWidget {
  static const String PATH = '/manageTags';
  static const String TITLE = 'Manage Tags';
  @override
  _ManageTagScreenState createState() => _ManageTagScreenState();
}

class _ManageTagScreenState extends State<ManageTagScreen> {
  List<Tag> tags = List();
  Future fetch() async {
    final String bKey = AppState.getInstance().business.key;
    await HttpUtil.getConveniently(
      url: Url.GET_TAGS,
      queryParameters: HttpUtil.formParams('businessKey', bKey),
      cb: (response) {
        List tagsMap = response.responseData;
        tags = tagsMap.map((i) => Tag.fromJson(i)).toList();
      }
    );
    return true;
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(ManageTagScreen.TITLE),
      ),
      body: FutureBuilder(
        future: fetch(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          } else {
            return Container(
              padding: EdgeInsets.all(10),
              child: Column(
                children: [
                  Container(
                    width: double.infinity,
                    child: OutlinedButton(
                      onPressed: () => Navigator.pushNamed(context, AddEditTagScreen.PATH, arguments: {
                        'viewOnly': false,
                        'tag': Tag(),
                      }),
                      child: Text('Add'),
                    ),
                  ),
                  SizedBox(height: 20),
                  Flexible(
                    child: Column(
                      children: [          
                        Flexible(
                          child: ListView.builder(
                            itemBuilder: (context, index) {
                              return TagWidget(
                                tag: tags[index],
                                removeTag: removeTag,
                              );
                            },
                            itemCount: tags.length
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            );
          }
        }
      ),
    );
  }

  void removeTag(Tag deletedTag) async {
    MapBuilder mb = HttpUtil.createMap();
    mb.merge(deletedTag.toJson());
    await HttpUtil.postConveniently(
      url: Url.REMOVE_TAG,
      data: mb.map,
      cb: (_) {
        setState(() {});
      }
    );
  }

}

class TagWidget extends StatelessWidget {
  final Tag tag;
  final Function(Tag) removeTag;
  TagWidget({this.tag, this.removeTag});
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          flex: 3,
          child: Container(
            width: double.infinity,
            child: Text(tag.name),
          ),
        ),
        DropdownButtonHideUnderline(
          child: DropdownButton(
            icon: Icon(Icons.menu),
            onChanged: (val) {
              if (val == 'edit') {
                Navigator.pushNamed(context, AddEditTagScreen.PATH, arguments: {
                  'viewOnly': false,
                  'tag': tag,
                });
              } else {
                UiUtil.showAlert(
                  title: 'Remove tag?',
                  content: Text("You won't be able to undo this action"),
                  onConfirm: () => removeTag(tag),
                );
              }
            },
            items: [
              DropdownMenuItem(
                value: 'edit',
                child: Text('edit'),
              ),
              DropdownMenuItem(
                value: 'delete',
                child: Text('delete'),
              ),
            ],
          )
        ),
      ],
    );
  }
}
