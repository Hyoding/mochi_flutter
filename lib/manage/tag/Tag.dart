class Tag {
  
  String key;
  String name;

  Tag({this.key, this.name});

  factory Tag.fromJson(Map<String, dynamic> json) {
    return Tag(
      key: json['key'],
      name: json['name'],
    );
  }

  Map<String, dynamic> toJson() => {
    'key': key,
    'name': name?.trim(),
  };

}