import 'package:flutter/material.dart';
import 'package:mochi_flutter/config/AppState.dart';
import 'package:mochi_flutter/config/Constants.dart';
import 'package:mochi_flutter/core/MapBuilder.dart';
import 'package:mochi_flutter/manage/tag/ManageTagScreen.dart';
import 'package:mochi_flutter/util/HttpUtil.dart';
import 'package:mochi_flutter/util/UiUtil.dart';
import 'package:mochi_flutter/util/Util.dart';

import 'Tag.dart';

class AddEditTagScreen extends StatefulWidget {
  static const String PATH = '/addEditTag';
  final bool viewOnly;
  final Tag tag;
  AddEditTagScreen({this.viewOnly, this.tag});
  @override
  _AddEditTagScreenState createState() => _AddEditTagScreenState(
    viewOnly: viewOnly,
    tag: tag,
  );
}

class _AddEditTagScreenState extends State<AddEditTagScreen> {
  final bool viewOnly;
  Tag tag;

  final _formKey = GlobalKey<FormState>();
  final TextEditingController tcName = TextEditingController();
  final TextEditingController tcPublicKey = TextEditingController();

  @override
  void initState() {
    super.initState();
    // clone
    Map tagMap = tag.toJson();
    tag = Tag.fromJson(tagMap);
  }

  _AddEditTagScreenState({this.viewOnly, this.tag});
  @override
  Widget build(BuildContext context) {
    final double fieldsPadding = 5;
    String title;
    if (tag.key == null) {
      title = 'Add Tag';
    } else {
      if (viewOnly) {
        title = 'View Tag';
      } else {
        title = 'Edit Tag';
      }
    }
    tcName.text = tag.name;
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text(title),
      ),
      body: Container(
        padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              SizedBox(height: fieldsPadding),
              TextFormField(
                validator: validateField,
                controller: tcName,
                onChanged: (val) => tag.name = val,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Name',
                ),
              ),
              SizedBox(height: fieldsPadding),
              Visibility(
                visible: !viewOnly,
                child: Container(
                  width: double.infinity,
                  child: OutlinedButton(
                    onPressed: () async {
                      if (!UiUtil.isFormReady(_formKey)) {
                        return;
                      }
                      if (tag.key == null) {
                        addNewTag();
                      } else {
                        editTag();
                      }
                    },
                    child: Text('Save'),
                  ),
                ),
              ),
            ],
          ),
        ),
      )
    );
  }

  void addNewTag() async {
    AppState appState = AppState.getInstance();
    MapBuilder mb = HttpUtil.createMap();
    mb.addEntry('businessKey', appState.business.key);
    mb.merge(tag.toJson());
    await HttpUtil.postConveniently(
      url: Url.ADD_TAG,
      data: mb.map,
      cb: (_) {
        Navigator.pop(context);
        Navigator.popAndPushNamed(context, ManageTagScreen.PATH);
      }
    );
  }

  void editTag() async {
    MapBuilder mb = HttpUtil.createMap();
    mb.merge(tag.toJson());
    await HttpUtil.postConveniently(
      url: Url.UPDATE_TAG,
      data: mb.map,
      cb: (_) {
        Navigator.pop(context);
        Navigator.popAndPushNamed(context, ManageTagScreen.PATH);      }
    );
  }
  
  String validatePhone(String phone) {
    if (Util.isPhoneNumber(phone)) {
      return null;
    }
    return 'Invalid';
  }

  String validateEmail(String email) {
    if (Util.isEmail(email)) {
      return null;
    }
    return 'Invalid';
  }

  String validateField(String priceStr) {
    if (priceStr == null || priceStr.isEmpty) {
      return "Cannot be empty";
    }
    return null;
  }
}