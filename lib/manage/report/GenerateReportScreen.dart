import 'package:flutter/material.dart';
import 'package:mochi_flutter/config/Constants.dart';
import 'package:mochi_flutter/core/MapBuilder.dart';
import 'package:mochi_flutter/util/HttpUtil.dart';

class GenerateReportScreen extends StatefulWidget {
  static const String PATH = '/generateReports';
  static const String TITLE = 'Reports';
  @override
  _GenerateReportScreenState createState() => _GenerateReportScreenState();
}

class _GenerateReportScreenState extends State<GenerateReportScreen> {
  int _radioValue = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(GenerateReportScreen.TITLE),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            Text(
              'For now we will generate report for entire history',
              style: TextStyle(fontSize: 14.0),
            ),
            Row(
              children: [
                Radio(
                  value: 0,
                  groupValue: _radioValue,
                  onChanged: _handleRadioValueChange,
                ),
                Text(
                  'Detailed Orders Report',
                  style: TextStyle(fontSize: 16.0),
                ),
              ],
            ),
            Row(
              children: [
                Radio(
                  value: 1,
                  groupValue: _radioValue,
                  onChanged: _handleRadioValueChange,
                ),
                Text(
                  'Trips Report',
                  style: TextStyle(fontSize: 16.0),
                ),
              ],
            ),
            Row(
              children: [
                Expanded(
                  child: OutlinedButton(
                    onPressed: requestReport,
                    child: Text('Email to me'),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void requestReport() {
    String report = '';
    switch (_radioValue) {
      case 0:
        report = 'DETAILED_ORDER';
        break;
      case 1:
        report = 'TRIPS';
        break;
    }
    MapBuilder mb = HttpUtil.createMap();
    mb.addEntry('reportType', report);
    HttpUtil.postConveniently(
      url: Url.REQUEST_REPORT,
      data: mb.map,
    );
  }

  void _handleRadioValueChange(int value) {
      setState(() {
        _radioValue = value;
      });
      print(_radioValue);
   }

}

