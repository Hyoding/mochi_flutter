import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:mochi_flutter/config/Constants.dart';
import 'package:mochi_flutter/core/MapBuilder.dart';
import 'package:mochi_flutter/core/model/Business.dart';
import 'package:mochi_flutter/login/ForgotPasswordScreen.dart';
import 'package:mochi_flutter/util/HttpUtil.dart';
import 'package:mochi_flutter/util/Util.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'AppConfigLoader.dart';
import 'RegisterScreen.dart';

class LoginScreen extends StatelessWidget {
  static const String PATH = '/login';
  static const String TITLE = 'Log in';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(TITLE),
      ),
      body: LoginWidget(),
      resizeToAvoidBottomPadding: false
    );
  }
}

class LoginWidget extends StatefulWidget {
  @override
  _LoginWidgetState createState() => _LoginWidgetState();
}

class _LoginWidgetState extends State<LoginWidget> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController tcEmail = TextEditingController();
  final TextEditingController tcPassword = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            SizedBox(height: 10),
            TextFormField(
              controller: tcEmail,
              validator: validateEmail,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Email',
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10),
              child: TextFormField(
                obscureText: true,
                enableSuggestions: false,
                autocorrect: false,
                controller: tcPassword,
                validator: validatePwd,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Password',
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10, bottom: 10),
              child: Container(
                width: double.infinity,
                child: OutlinedButton(
                  onPressed: onClickLogin,
                  child: Text('Log In'),
                ),
              ),
            ),
            Center(
              child: FlatButton(
                onPressed: () => Get.to(ForgotPasswordScreen()),
                child: Text(
                  'Forgot password?',
                  style: TextStyle(color: Colors.blue),
                ),
              )
            ),
            Center(
              child: FlatButton(
                onPressed: () => Get.to(RegisterScreen()),
                child: Text(
                  'Not registered your business yet?',
                  style: TextStyle(color: Colors.blue),
                ),
              )
            ),
          ],
          
        ),
      ),
    );
  }

  void onClickLogin() {
    if (!_formKey.currentState.validate()) {
      return;
    }

    MapBuilder mb = HttpUtil.createMap();
    mb.addEntry('email', tcEmail.text);
    mb.addEntry('password', tcPassword.text);
    HttpUtil.getConveniently(
      url: Url.VALIDATE_LOGIN,
      queryParameters: mb.map,
      cb: (myResponse) {
        fetchAppConfigForBusinessAndLogIn();
      },
    );
  }

  void fetchAppConfigForBusinessAndLogIn() async {
    await AppConfigLoader.loadAppData(
      userEmail: tcEmail.text,
    );

    final prefs = await SharedPreferences.getInstance();
    prefs.setString(Constants.STORAGE_USER_EMAIL, tcEmail.text);

    Get.back();
    String initScreent = await AppConfigLoader.initScreen();
    Navigator.pushNamedAndRemoveUntil(context, initScreent, (r) => false);
  }

  String validatePwd(String pwd) {
    if (pwd.isEmpty) {
      return 'Cannot be empty';
    }
    return null;
  }

  String validateEmail(String email) {
    if (Util.isEmail(email)) {
      return null;
    }
    return 'Invalid email';
  }
}

class BusinessNameButtonWidget extends StatelessWidget {
  final Business business;
  final Function onPress;
  BusinessNameButtonWidget({this.business, this.onPress});
  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: () => onPress(business.key),
      child: Center(
        child: Text(
          business.name,
          style: TextStyle(
            fontSize: 18,
          ),
        ),
      ),
    );
  }
}