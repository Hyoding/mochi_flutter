import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mochi_flutter/config/Constants.dart';
import 'package:mochi_flutter/core/MapBuilder.dart';
import 'package:mochi_flutter/core/model/Address.dart';
import 'package:mochi_flutter/core/model/Business.dart';
import 'package:mochi_flutter/util/HttpUtil.dart';
import 'package:mochi_flutter/util/UiUtil.dart';
import 'package:mochi_flutter/util/Util.dart';

class RegisterScreen extends StatelessWidget {
  static const String PATH = '/register';
  static const String TITLE = 'Register';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(TITLE),
      ),
      body: RegisterWidget(),
    );
  }
}

class RegisterWidget extends StatefulWidget {
  @override
  _RegisterWidgetState createState() => _RegisterWidgetState();
}

class _RegisterWidgetState extends State<RegisterWidget> {
  final _formKey = GlobalKey<FormState>();

  // business
  final TextEditingController tcBusinessName = TextEditingController();
  final TextEditingController tcBusinessPhone = TextEditingController();

  final TextEditingController tcAddressStreet = TextEditingController();
  final TextEditingController tcAddressUnit = TextEditingController();
  final TextEditingController tcAddressCity = TextEditingController();
  final TextEditingController tcAddressProvince = TextEditingController();
  final TextEditingController tcAddressCountry = TextEditingController();
  final TextEditingController tcAddressPostalCode = TextEditingController();

  // owner
  final TextEditingController tcFirstName = TextEditingController();
  final TextEditingController tcLastName = TextEditingController();
  final TextEditingController tcPhoneNumber = TextEditingController();
  final TextEditingController tcEmail = TextEditingController();
  final TextEditingController tcPwd = TextEditingController();
  final TextEditingController tcPwdConf = TextEditingController();
  
  @override
  Widget build(BuildContext context) {
    final double fieldsPadding = 5;
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              SizedBox(height: fieldsPadding),
              Container(
                padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                child: Column(
                  children: [
                    Text('Business', textScaleFactor: 1.2, textAlign: TextAlign.left,),
                    SizedBox(height: fieldsPadding),
                    TextFormField(
                      controller: tcBusinessName,
                      validator: validateField,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Name',
                      ),
                    ),
                    SizedBox(height: fieldsPadding),
                    TextFormField(
                      controller: tcBusinessPhone,
                      validator: validatePhone,
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Phone number',
                      ),
                    ),
                    Divider(),
                    TextFormField(
                      controller: tcAddressStreet,
                      validator: validateField,
                      decoration: InputDecoration(
                        hintText: 'e.g. 123 Main Street',
                        border: OutlineInputBorder(),
                        labelText: 'Street address',
                      ),
                    ),
                    SizedBox(height: fieldsPadding),
                    TextFormField(
                      controller: tcAddressUnit,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Unit',
                      ),
                    ),
                    SizedBox(height: fieldsPadding),
                    TextFormField(
                      controller: tcAddressCity,
                      validator: validateField,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'City',
                      ),
                    ),
                    SizedBox(height: fieldsPadding),
                    TextFormField(
                      controller: tcAddressProvince,
                      validator: validateField,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Province',
                      ),
                    ),
                    SizedBox(height: fieldsPadding),
                    TextFormField(
                      controller: tcAddressCountry,
                      validator: validateField,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Country',
                      ),
                    ),
                    SizedBox(height: fieldsPadding),
                    TextFormField(
                      controller: tcAddressPostalCode,
                      validator: validateField,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Postal code',
                      ),
                    ),
                  ],
                ),
              ),

              Container(
                padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                child: Column(
                  children: [
                    Text('Owner', textScaleFactor: 1.2, textAlign: TextAlign.left,),
                    SizedBox(height: fieldsPadding),
                    TextFormField(
                      controller: tcFirstName,
                      validator: validateField,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'First name',
                      ),
                    ),
                    SizedBox(height: fieldsPadding),
                    TextFormField(
                      controller: tcLastName,
                      validator: validateField,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Last name',
                      ),
                    ),
                    SizedBox(height: fieldsPadding),
                    TextFormField(
                      controller: tcPhoneNumber,
                      validator: validatePhone,
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Phone number',
                      ),
                    ),

                    SizedBox(height: fieldsPadding),
                    TextFormField(
                      controller: tcEmail,
                      validator: validateEmail,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Email',
                      ),
                    ),
                    SizedBox(height: fieldsPadding),
                    TextFormField(
                      obscureText: true,
                      enableSuggestions: false,
                      autocorrect: false,
                      controller: tcPwd,
                      validator: validateField,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Password',
                      ),
                    ),
                    SizedBox(height: fieldsPadding),
                    TextFormField(
                      obscureText: true,
                      enableSuggestions: false,
                      autocorrect: false,
                      controller: tcPwdConf,
                      validator: validateField,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Confirm Password',
                      ),
                    ),
                  ],
                ),
              ),
              
              
              Padding(
                padding: EdgeInsets.only(top: 10, bottom: 10),
                child: Container(
                  width: double.infinity,
                  child: OutlinedButton(
                    onPressed: register,
                    child: Text('Register'),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void register() {
    if (!_formKey.currentState.validate()) {
      return;
    }
    if (!(tcPwd.text == tcPwdConf.text)) {
      UiUtil.showAlert(
        content: Text('Passwords are not matching')
      );
    }
    Address businessAddress = Address(
      streetAddress: tcAddressStreet.text,
      unit: tcAddressUnit.text,
      city: tcAddressCity.text,
      province: tcAddressProvince.text,
      country: tcAddressCountry.text,
    );
    Business business = Business(
      name: tcBusinessName.text,
      phoneNumber: tcBusinessPhone.text,
      address: businessAddress,
    );
    MapBuilder mb = HttpUtil.createMap();
    mb.merge(business.toJson());
    mb.addEntry('employeeFirstName', tcFirstName.text?.trim());
    mb.addEntry('employeeLastName', tcLastName.text?.trim());
    mb.addEntry('employeeEmail', tcEmail.text?.trim());
    mb.addEntry('employeePhoneNumber', tcPhoneNumber.text?.trim());
    mb.addEntry('employeePassword', tcPwd.text?.trim());
    HttpUtil.postConveniently(
      data: mb.map,
      url: Url.REGISTER,
      cb: (_) {
        Get.back();
      },
    );
  }

  String validateField(String priceStr) {
    if (priceStr == null || priceStr.isEmpty) {
      return "Cannot be empty";
    }
    return null;
  }

  String validatePhone(String phone) {
    if (Util.isPhoneNumber(phone)) {
      return null;
    }
    return 'Invalid';
  }

  String validateEmail(String email) {
    if (Util.isEmail(email)) {
      return null;
    }
    return 'Invalid email';
  }
  
}