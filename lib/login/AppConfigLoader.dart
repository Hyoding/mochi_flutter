import 'package:flutter/cupertino.dart';
import 'package:mochi_flutter/config/AppState.dart';
import 'package:mochi_flutter/config/Constants.dart';
import 'package:mochi_flutter/core/MapBuilder.dart';
import 'package:mochi_flutter/orders/OrdersScreen.dart';
import 'package:mochi_flutter/trips/MyTripsScreen.dart';
import 'package:mochi_flutter/util/HttpUtil.dart';
import 'package:mochi_flutter/util/Util.dart';

import 'LoginScreen.dart';
import 'NoConnectionScreen.dart';

class AppConfigLoader {
  AppConfigLoader._();

  static Future loadAppData({@required String userEmail}) async {
    MapBuilder mb = HttpUtil.createMap();
    mb.addEntry('email', userEmail);
    await HttpUtil.getConveniently(
      url: Url.GET_MOBILE_APP_CONFIG,
      queryParameters: mb.map,
      cb: (myResponse) {
        AppState appState = AppState.fromJson(myResponse.responseData);
        AppState.setInstance(appState);
        print('loaded app config');
        return;
      },
    );
  }

  static Future reloadAppData() async {
    String email = await Util.getMyEmail();
    loadAppData(userEmail: email);
  }

  static Future<bool> ping() async {
    try {
      await HttpUtil.getConveniently(url: Url.PING, cb: null, showErrMsg: false);
      return true;
    } catch (ex) {
      return false;
    }
  }

  static Future<String> initScreen() async {
    String initialScreen;
    if (await Util.isLoggedIn()) {
      final email = await Util.getLocalStorage(Constants.STORAGE_USER_EMAIL);
      try {
        await loadAppData(userEmail: email);
        AppState appState = AppState.getInstance();
        if (appState.employee.type == 'Driver') {
          initialScreen = MyTripScreen.PATH;
        } else {
          initialScreen = OrdersScreen.PATH;
        }
      } catch (ex) {
        await Util.removeLoginStorage();
        initialScreen = NoConnectionScreen.PATH;
      }
    } else {
      initialScreen = LoginScreen.PATH;
    }
    return initialScreen;
  }

}