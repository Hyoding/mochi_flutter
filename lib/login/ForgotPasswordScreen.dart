import 'package:flutter/material.dart';
import 'package:mochi_flutter/config/Constants.dart';
import 'package:mochi_flutter/util/HttpUtil.dart';
import 'package:mochi_flutter/util/Util.dart';

class ForgotPasswordScreen extends StatelessWidget {
  static const String PATH = '/resetPassword';
  static const String TITLE = 'Reset Password';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(TITLE),
      ),
      body: ForgotPasswordWidget(),
    );
  }
}

class ForgotPasswordWidget extends StatefulWidget {
  @override
  _ForgotPasswordWidgetState createState() => _ForgotPasswordWidgetState();
}

class _ForgotPasswordWidgetState extends State<ForgotPasswordWidget> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController tcEmail = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            SizedBox(height: 10),
            TextFormField(
              controller: tcEmail,
              validator: validateEmail,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Email',
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10, bottom: 10),
              child: Container(
                width: double.infinity,
                child: OutlinedButton(
                  onPressed: sendInstructions,
                  child: Text('Send me instructions'),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void sendInstructions() {
    if (!_formKey.currentState.validate()) {
      return;
    }
    HttpUtil.postConveniently(
      url: Url.RESET_PWD + '?email=${tcEmail.text}',
      cb: (_) {
        setState(() {
          tcEmail.text = '';
        });
      },
    );
  }

  String validateEmail(String email) {
    if (Util.isEmail(email)) {
      return null;
    }
    return 'Invalid email';
  }
  
}