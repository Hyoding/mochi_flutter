import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mochi_flutter/config/Constants.dart';
import 'package:mochi_flutter/login/AppConfigLoader.dart';

class NoConnectionScreen extends StatefulWidget {
  static const String PATH = '/noConnection';
  @override
  _NoConnectionScreenState createState() => _NoConnectionScreenState();
}

class _NoConnectionScreenState extends State<NoConnectionScreen> {
  Timer timer;
  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(Duration(seconds: 10), checkConnection);
  }
  @override
  void dispose() {
    super.dispose();
    timer?.cancel();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Constants.APP_NAME),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Trying to connect to server', style: TextStyle(
              fontSize: 20,
            )),
            SizedBox(height: 20),
            CircularProgressIndicator(),
          ],
        ),
      ),
    );
  }

  void checkConnection(Timer timer) async {
    print('checking connection');
    bool good = await AppConfigLoader.ping();
    print(good);
    if (good) {
      String screen = await AppConfigLoader.initScreen();
      Navigator.pushNamedAndRemoveUntil(context, screen, (r) => false);
    }
  }

}