import 'package:flutter/material.dart';
import 'package:location_permissions/location_permissions.dart';
import 'package:mochi_flutter/config/AppState.dart';
import 'package:mochi_flutter/core/model/Business.dart';
import 'package:mochi_flutter/login/LoginScreen.dart';
import 'package:mochi_flutter/manage/employee/Employee.dart';
import 'package:mochi_flutter/util/Util.dart';

import '../core/components/BaseDrawerScreen.dart';

class SettingsScreen extends BaseDrawerScreen {
  static const String PATH = '/settings';
  static const String TITLE = 'Settings';
  @override
  Widget getBody(BuildContext context) {
    AppState config = AppState.getInstance();
    Employee emp = config.employee;
    Business business = config.business;
    return Scaffold(
      body: Column(
        children: [
          Flexible(
            flex: 10,
            child: Column(
              children: [
                Flexible(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        emp.toString(),
                        style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 5),
                        child: Text(emp.email),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 5),
                        child: Text(emp.phoneNumber),
                      ),
                    ],
                  ),
                ),
                Flexible(
                  flex: 3,
                  child: Container(
                    padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
                    child: Column(
                      children: [
                        Divider(thickness: 1),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Business',
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 18,
                              ),
                            ),
                            Text(
                              business.name,
                              style: TextStyle(
                                fontSize: 18,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 20),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Title',
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 18,
                              ),
                            ),
                            Text(
                              emp.type,
                              style: TextStyle(
                                fontSize: 18,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Location Settings',
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 18,
                              ),
                            ),
                            IconButton(
                              icon: const Icon(Icons.settings, color: Colors.blue),
                              onPressed: () {
                                LocationPermissions().openAppSettings().then((bool hasOpened) =>
                                    debugPrint('App Settings opened: ' + hasOpened.toString()));
                              },
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Flexible(
            child: Container(
              height: double.infinity,
              width: double.infinity,
              child: OutlinedButton(
                onPressed: () => logout(context),
                child: Text(
                  'LOGOUT',
                  style: TextStyle(color: Colors.red),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  String getTitle() {
    return TITLE;
  }

  Future<void> logout(context) async {
    await Util.removeLoginStorage();
    Navigator.pushNamedAndRemoveUntil(context, LoginScreen.PATH, (r) => false);
  }

}
