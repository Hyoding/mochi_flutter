import 'dart:async';

import 'package:badges/badges.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:get/get.dart';
import 'package:mapbox_search_flutter/mapbox_search_flutter.dart';
import 'package:mochi_flutter/config/AppState.dart';
import 'package:mochi_flutter/config/Constants.dart';
import 'package:mochi_flutter/core/MapBuilder.dart';
import 'package:mochi_flutter/core/components/Drawer.dart';
import 'package:mochi_flutter/core/model/Address.dart';
import 'package:mochi_flutter/core/model/Coordinate.dart';
import 'package:mochi_flutter/login/AppConfigLoader.dart';
import 'package:mochi_flutter/manage/tag/Tag.dart';
import 'package:mochi_flutter/orders/Order.dart';
import 'package:mochi_flutter/trips/Trip.dart';
import 'package:mochi_flutter/util/HttpUtil.dart';
import 'package:email_validator/email_validator.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:mochi_flutter/util/UiUtil.dart';
import 'package:mochi_flutter/util/Util.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter/services.dart';
import 'package:latlong/latlong.dart';

class OrdersScreen extends StatelessWidget {
  static const String PATH = '/orders';
  static const String TITLE = 'Orders';
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        drawer: MyDrawer(),
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Tab(text: 'ACTIVE', icon: Icon(Icons.emoji_transportation)),
              Tab(text: 'TRACK', icon: Icon(Icons.map)),
              Tab(text: 'HISTORY', icon: Icon(Icons.history)),
            ],
          ),
          title: Text(TITLE),
        ),
        body: TabBarView(
          physics: NeverScrollableScrollPhysics(),
          children: [
            ActiveOrdersTab(),
            TrackTab(),
            HistoryTab(),
          ],
        ),
      ),
    );
  }
}

class TrackTab extends StatefulWidget {
  @override
  _TrackTabState createState() => _TrackTabState();
}

class _TrackTabState extends State<TrackTab> {
  Trip selectedTrip;
  List<Trip> trips = List();
  List<Order> orders = List();
  DateTime lastUpdated;
  @override
  void initState() {
    super.initState();
    print('track tab init');
  }
  @override
  void dispose() {
    super.dispose();
    print('track tab dispose');
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder(
        future: getTrackingData(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          } else {
            return Column(
              children: [
                header(),
                Flexible(
                  flex: 10,
                  child: showTrackingMap(),
                ),
              ],
            );
          }
        }
      ),
    );
  }

  Widget header() {
    return ExpandablePanel(
      header: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text(
            "Last updated: ${Util.formatShortDate(lastUpdated)}",
            textScaleFactor: 1.2,
          ),
          IconButton(icon: Icon(Icons.refresh), onPressed: () {
            setState((){});
            Get.snackbar('Success', 'Updated', snackPosition: SnackPosition.BOTTOM);
          }),
        ],
      ),
      expanded: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.indeterminate_check_box,
                size: 20,
                color: Colors.blue
              ),
              SizedBox(width: 5),
              Flexible(
                child: Text('Waiting'),
              ),
              SizedBox(width: 15),
              Icon(
                Icons.circle,
                size: 20,
                color: Colors.blue
              ),
              SizedBox(width: 5),
              Flexible(
                child: Text('Delivering'),
              ),
            ],
          ),
          SizedBox(height: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.dehaze,
                size: 15,
                color: Colors.red
              ),
              SizedBox(width: 5),
              Flexible(
                child: Text('Late'),
              ),
              SizedBox(width: 15),
              Icon(
                Icons.dehaze,
                size: 15,
                color: Colors.orange
              ),
              SizedBox(width: 5),
              Flexible(
                child: Text('Upcoming'),
              ),
              SizedBox(width: 15),
              Icon(
                Icons.dehaze,
                size: 15,
                color: Colors.black
              ),
              SizedBox(width: 5),
              Flexible(
                child: Text('Normal'),
              ),
            ],
          ),
          SizedBox(height: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.drive_eta,
                size: 25,
                color: Colors.purple
              ),
              SizedBox(width: 5),
              Flexible(
                child: Text('En route'),
              ),
              SizedBox(width: 15),
              Icon(
                Icons.drive_eta,
                size: 25,
                color: Colors.grey
              ),
              SizedBox(width: 5),
              Flexible(
                child: Text('Returning'),
              ),
            ],
          ),
          SizedBox(height: 5),
        ],
      ),
    );
  }

  Future getTrackingData() async {
    await HttpUtil.getConveniently(
      url: Url.GET_ACTIVE_SUMMARY,
      queryParameters: HttpUtil.formParams('businessKey', AppState.getInstance().business.key),
      cb: (myResponse) {
        Map<String, dynamic> json = myResponse.responseData;
        List tripsMap = json['trips'];
        trips = tripsMap.map((i) => Trip.fromJson(i)).toList();
        List ordersMap = json['activeOrders'];
        orders = ordersMap.map((i) => Order.fromJson(i)).toList();
      },
    );
    lastUpdated = DateTime.now();
    return true;
  }

  Widget showTrackingMap() {
    AppState appState = AppState.getInstance();
    Coordinate businessLoc = appState.business.address.coordinate;
    List<Marker> empMarkers = List();
    empMarkers.add(Marker(
      point: LatLng(businessLoc.latitude, businessLoc.longitude),
      builder: (ctx) => Icon(Icons.business),
    ));
    if (selectedTrip != null) {
      empMarkers.add(getDriverMarker(selectedTrip));
      empMarkers.addAll(selectedTrip.orderSequence.map((os) {
        Order order = selectedTrip.orders.where((o) => o.key == os.orderKey).first;
        return getDriverOrderMarker(order, os.sequence + 1);
      }).toList());
    } else {
      empMarkers.addAll(getActiveOrdersMakers());
      empMarkers.addAll(trips.map((trip) => getDriverMarker(trip)).toList());
    }
    return FlutterMap(
      options: MapOptions(	
        center: LatLng(businessLoc.latitude, businessLoc.longitude),
        zoom: 12.0,
      ),
      layers: [
        TileLayerOptions(
          urlTemplate: appState.mapboxStyle,
          additionalOptions: {
            'accessToken': appState.mapboxKey,
            'id': 'mapbox.streets',
          },
        ),
        MarkerLayerOptions(
          markers: empMarkers,
        ),
      ],
    );
  }

  List<Marker> getActiveOrdersMakers() {
    return orders.map((order) {
      IconData iconData = order.status == 'Accepted' ? Icons.indeterminate_check_box : Icons.circle;
      bool isDelivered = order.status == 'Delivered' ? true : false;
      Coordinate coord = order.address.coordinate;
      return Marker(
        point: LatLng(coord.latitude, coord.longitude),
        builder: (ctx) => GestureDetector(
          onLongPress: () {
            openOrderDialog(
              context: ctx,
              selectedOrder: order,
              viewOnly: false,
              saveOrder: saveOrder,
            );
          },
          onTap: () {
            openOrderDialog(
              context: ctx,
              selectedOrder: order,
              viewOnly: true,
            );
          },
          child: Icon(
            iconData,
            size: 15,
            color: isDelivered ? Colors.grey : getOrderColour(order),
          ),
        ),
      );
    }).toList();
  }

  Future saveOrder(Order selectedOrder) async {
    MapBuilder mb = HttpUtil.createMap();
    mb.merge(selectedOrder.toJson());
    String url;
    if (selectedOrder.key == null) { // new
      mb.addEntry('businessKey', AppState.getInstance().business.key);
      url = Url.CREATE_ORDER;
    } else { // edit
      url = Url.UPDATE_ORDER;
    }
    await HttpUtil.postConveniently(
      url: url,
      data: mb.map,
    );
    setState((){});
  }

  Marker getDriverOrderMarker(Order order, int seq) {
    Coordinate coord = order.address.coordinate;
    var isCanceledOrDelivered = order.status != 'Out for delivery';
    return Marker(
      point: LatLng(coord.latitude, coord.longitude),
      builder: (ctx) => GestureDetector(
        onLongPress: () {
          openOrderDialog(
            context: ctx,
            selectedOrder: order,
            viewOnly: false,
            saveOrder: saveOrder,
          );
        },
        onTap: () {
          openOrderDialog(
            context: ctx,
            selectedOrder: order,
            viewOnly: true
          );
        },
        child: Badge(
          badgeColor: isCanceledOrDelivered ? Colors.grey : getOrderColour(order) == null ? Colors.black : getOrderColour(order),
          position: BadgePosition.topEnd(top: 12, end: 15),
          badgeContent: seq == null ? null : Text((seq).toString(),
            style: TextStyle(
              color: Colors.white
            ),
          ),
          child: Text('')
        ),
      ),
    );
  }

  Marker getDriverMarker(Trip trip) {
    Coordinate empLoc = trip.location;
    bool completedAllOrders = true;
    trip.orders.forEach((order) {
      if (order.status != 'Delivered') {
        completedAllOrders = false;
        return;
      }
    });
    return Marker(
      point: LatLng(empLoc.latitude, empLoc.longitude),
      builder: (ctx) => Badge(
        position: BadgePosition.bottomStart(bottom: 20, start: 20),
        badgeColor: completedAllOrders ? Colors.grey : Colors.purple,
        badgeContent: Text(
          trip.employeeInitial,
          textScaleFactor: 0.8,
          style: TextStyle(color: Colors.white),
        ),
        child: GestureDetector(
          onLongPress: () {
            UiUtil.showCallOrTextAlert(trip.employeePhone);
          },
          onTap: () {          
            setState(() {
              selectedTrip = selectedTrip == null ? trip : null;
            });
          },
          child: Icon(Icons.drive_eta, color: completedAllOrders ? Colors.grey : Colors.purple),
        ),
      ),
    );
  }

}

class HistoryTab extends StatefulWidget {
  @override
  _HistoryTabState createState() => _HistoryTabState();
}

class _HistoryTabState extends State<HistoryTab> {
  List<Order> orders;
  RefreshController _refreshController = RefreshController(initialRefresh: false);
  void _onRefresh() async{
    print('onrefresh');
    setState(() {
    });
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    print('onloading');
    await Future.delayed(Duration(milliseconds: 1000));
    if(mounted)
    setState(() {

    });
    _refreshController.loadComplete();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder(
        future: fetchHistoryOrders(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          } else {
            final String yyyyMMdd = 'MMM dd, yyyy';
            List<String> distinctDates = orders.map((o) => DateFormat(yyyyMMdd).format(o.createdDate)).toSet().toList();
            return SmartRefresher(
              enablePullDown: true,
              controller: _refreshController,
              onRefresh: _onRefresh,
              onLoading: _onLoading,
              child: ListView.separated(
                itemBuilder: (context, index) {
                  return Container(
                    child: OrderHistoryByDate(
                      yyyyMMdd: distinctDates[index],
                      orders: orders.where((o) => DateFormat(yyyyMMdd).format(o.createdDate) == distinctDates[index]).toList(),
                    ),
                  );
                },
                separatorBuilder: (_,__) => Divider(thickness: 1),
                itemCount: distinctDates.length
              ),
            );
          }
        },
      ),
    );
  }

  Future fetchHistoryOrders() async {
    final String bKey = AppState.getInstance().business.key;
    await HttpUtil.getConveniently(
      url: Url.GET_ORDER_HISTORY,
      queryParameters: HttpUtil.formParams('businessKey', bKey),
      cb: (myResponse) {
        List ordersMap = myResponse.responseData;
        orders = ordersMap.map((i) => Order.fromJson(i)).toList();
      },
    );
    return orders;
  }
}

class ActiveOrdersTab extends StatefulWidget {
  @override
  _ActiveOrdersTabState createState() => _ActiveOrdersTabState();
}

class _ActiveOrdersTabState extends State<ActiveOrdersTab> {
  List<Order> orders;
  AppState appState = AppState.getInstance();
  Timer timer;
  RefreshController _refreshController = RefreshController(initialRefresh: false);
  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(Duration(minutes: 1), (_) => setState(() {}));
    AppConfigLoader.reloadAppData();
  }
  @override
  void dispose() {
    super.dispose();
    timer?.cancel();
  }
  void _onRefresh() async{
    setState(() {
    });
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    await Future.delayed(Duration(milliseconds: 1000));
    if(mounted)
    setState(() {
    });
    _refreshController.loadComplete();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () => openOrderDialog(
          selectedOrder: null,
          context: context,
          viewOnly: false,
          saveOrder: saveOrder,
        ),
        tooltip: 'Add a new order',
        child: Icon(Icons.add),
      ),
      body: FutureBuilder(
        future: fetchActiveOrders(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          } else {
          return Column(
              children: [
                Flexible(
                  child: SmartRefresher(
                  enablePullDown: true,
                  controller: _refreshController,
                  onRefresh: _onRefresh,
                  onLoading: _onLoading,
                    child:ListView.separated(
                      itemBuilder: (context, index) {
                        return FlatButton(
                          onPressed: () {
                            openOrderDialog(
                              context: context,
                              selectedOrder: orders[index],
                              viewOnly: true,
                            );
                          },
                          child: Container(
                            child: NeedToDeliverOrderWidget(
                              order: orders[index],
                              onChanged: (val) {
                                Order selectedOrder = orders[index];
                                if (val == 'edit') {
                                  openOrderDialog(
                                    context: context,
                                    selectedOrder: selectedOrder,
                                    viewOnly: false,
                                    saveOrder: saveOrder,
                                  );
                                } else if (val == 'reject') {
                                  UiUtil.showAlert(
                                    title: 'Reject order?',
                                    content: Text("You won't be able to undo this action"),
                                    onConfirm: () => rejectOrder(selectedOrder),
                                  );
                                }
                              },
                            ),
                          ),
                        );
                      },
                      separatorBuilder: (_,__) => Divider(thickness: 1),
                      itemCount: orders.length
                    ),
                  ),
                ),
                SizedBox(height: 75),
              ],
            );
          }
        },
      ),
    );
  }

  Future fetchActiveOrders() async {
    final String bKey = AppState.getInstance().business.key;
    await HttpUtil.getConveniently(
      url: Url.GET_ACTIVE_ORDERS,
      queryParameters: HttpUtil.formParams('businessKey', bKey),
      cb: (myResponse) {
        List ordersMap = myResponse.responseData;
        orders = ordersMap.map((i) => Order.fromJson(i)).toList();
        print('get orders to deliver');
      },
    );
    return orders;
  }

  void rejectOrder(Order selectedOrder) async {
    MapBuilder mb = HttpUtil.createMap();
    mb.merge(selectedOrder.toJson());
    await HttpUtil.postConveniently(
      url: Url.REJECT_ORDER,
      data: mb.map,
    );
    setState(() {});
  }

  void saveOrder(Order selectedOrder) async {
    MapBuilder mb = HttpUtil.createMap();
    mb.merge(selectedOrder.toJson());
    String url;
    if (selectedOrder.key == null) { // new
      mb.addEntry('businessKey', AppState.getInstance().business.key);
      url = Url.CREATE_ORDER;
    } else { // edit
      url = Url.UPDATE_ORDER;
    }
    await HttpUtil.postConveniently(
      url: url,
      data: mb.map,
    );
    setState((){});
  }
}

class NeedToDeliverOrderWidget extends StatelessWidget {
  final Order order;
  final Function(String) onChanged;
  NeedToDeliverOrderWidget({this.order, this.onChanged});
  @override
  Widget build(BuildContext context) {
    bool isOnTheWay = order.status == 'Out for delivery';
    MaterialColor textColour = getOrderColour(order);
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: EdgeInsets.only(right: 10),
            child: isOnTheWay ?
              Icon(Icons.airport_shuttle_outlined) : 
              Icon(Icons.schedule),
          ),
          Expanded(
            child: Text(
              shortAddress(order.address)  + ", " + order.address.city,
              style: TextStyle(color: textColour),
            ),
          ),
          Text(
            Util.formatShortDate(order.deliverBy),
            style: TextStyle(color: textColour, ),
          ),
          DropdownButtonHideUnderline(
            child: DropdownButton(
              icon: Icon(Icons.menu),
              onChanged: (val) => onChanged(val),
              items: [
                DropdownMenuItem(
                  value: 'edit',
                  child: Text('edit'),
                ),
                DropdownMenuItem(
                  value: 'reject',
                  child: Text('reject'),
                ),
              ],
            )
          ),
        ],
      ),
    );
  }
}

class OrderHistoryByDate extends StatelessWidget {
  final String yyyyMMdd;
  final List<Order> orders;
  OrderHistoryByDate({
    @required this.yyyyMMdd,
    @required this.orders
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(top: 10, bottom: 10),
            child: Text(yyyyMMdd),
          ),
          ListView.separated(
            shrinkWrap: true,
            physics: ClampingScrollPhysics(),
            itemBuilder: (context, index) {
              return FlatButton(
                onPressed: () {
                  openOrderDialog(
                    context: context,
                    selectedOrder: orders[index],
                    viewOnly: true,
                  );
                },
                child: Container(
                  child: OrderHistoryWidget(
                    order: orders[index],
                  ),
                ),
              );
            },
            separatorBuilder: (_,__) => Divider(thickness: 1),
            itemCount: orders.length
          ),
        ],
      ),
    );
  }
}

class OrderHistoryWidget extends StatelessWidget {
  final Order order;
  OrderHistoryWidget({this.order});
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Text(shortAddress(order.address))
          ),
          Text(Util.formatShortDate(order.deliverBy)),
          Flexible(
            child: Text(
              order.status,
              style: TextStyle(
                color: order.status == 'Canceled' || order.status == 'Rejected' ? Colors.red : null,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class BasicDateTimeField extends StatelessWidget {
  final format = Util.dateFormat;
  final bool readOnly;
  final FormFieldValidator<DateTime> validator;
  final TextEditingController controller;
  final Function(DateTime) successCb;
  BasicDateTimeField({this.validator, this.successCb, this.controller, this.readOnly});
  @override
  Widget build(BuildContext context) {
    if (successCb == null) {
      throw Exception('successCb must be provided');
    }
    DateTime dateTimeNow = DateTime.now();
    DateTime showTime = dateTimeNow.add(Duration(minutes: AppState.getInstance().defaultETA));
    return DateTimeField(
      enabled: readOnly,
      validator: validator,
      controller: controller,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Estimated delivery time',
      ),
      format: format,
      onShowPicker: (context, currentValue) async {
        final date = await showDatePicker(
            context: context,
            firstDate: dateTimeNow,
            initialDate: dateTimeNow,
            lastDate: DateTime(2100));
        if (date != null) {
          final time = await showTimePicker(
            context: context,
            initialTime:
                TimeOfDay.fromDateTime(currentValue ?? showTime),
          );
          DateTime selectedDateTime = DateTimeField.combine(date, time);
          successCb(selectedDateTime);
          return selectedDateTime;
        } else {
          successCb(currentValue);
          return currentValue;
        }
      },
    );
  }
}

Future openOrderDialog({
    BuildContext context,
    Order selectedOrder,
    bool viewOnly,
    Function(Order) saveOrder,
    bool disableMenu = false,
  }) async {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController tcAddress = TextEditingController();
  final TextEditingController tcUnit = TextEditingController();
  final TextEditingController tcPhone = TextEditingController();
  final TextEditingController tcEmail = TextEditingController();
  final TextEditingController tcEDT = TextEditingController();
  final TextEditingController tcNote = TextEditingController();
  String validatePhone(String phone) {
    if (tcPhone.text.isEmpty && tcEmail.text.isEmpty) {
      return 'Need email or phone';
    }

    if (tcEmail.text.isNotEmpty && phone.isEmpty) {
      return null;
    }
    Pattern pattern = r'^(?:[+0]9)?[0-9]{10}$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(phone)) {
      return 'Invalid Phone Number';
    }
    return null;
  }

  String validateEmail(String email) {
    if (tcPhone.text.isEmpty && tcEmail.text.isEmpty) {
      return 'Need email or phone';
    }

    if (tcPhone.text.isNotEmpty && email.isEmpty) {
      return null;
    }
    if (EmailValidator.validate(email)) {
      return null;
    }
    return "Invalid email";
  }

  String dialogTitle;
  if (viewOnly) {
    dialogTitle = 'View';
  } else {
    dialogTitle = selectedOrder == null ? 'Add' : 'Edit';
  }
  AppState appState = AppState.getInstance();
  Map<Tag, bool> tagSelections = Map();
  if (selectedOrder != null) {
    Address address = selectedOrder.address;
    tcAddress.text = address.streetAddress;
    tcUnit.text = address.unit;
    tcPhone.text = selectedOrder.phoneNumber;
    tcEmail.text = selectedOrder.email;
    tcEDT.text = Util.formatDate(selectedOrder.deliverBy);
    tcNote.text = selectedOrder.note;
    appState.tags.forEach((tag) {
      final index = selectedOrder.tags.indexWhere((t) => tag.key == t.key);
      if (index >= 0) {
        tagSelections.putIfAbsent(tag, () => true);  
      } else {
        tagSelections.putIfAbsent(tag, () => false);
      }
    });
  } else {
    selectedOrder = Order(address: Address(), tags: List());
    appState.tags.forEach((tag) { 
      tagSelections.putIfAbsent(tag, () => false);
    });
  }

  await showDialog(
    barrierDismissible: viewOnly,
    context: context,
    builder: (BuildContext context) {
      return StatefulBuilder(
        builder: (context, setState) {
          List selectedTagsList = List();
          tagSelections.forEach((tag, isSelected) { 
            if (isSelected) {
              selectedTagsList.add(tag.name);
            }
          });
          String selectedTags = selectedTagsList.join(',');
          return Form(
            key: _formKey,
            child: SimpleDialog(
              contentPadding: EdgeInsets.fromLTRB(10, 12.0, 10, 16.0),
              title: Center(
                child: Text(dialogTitle)
              ),
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                  child: TextFormField(
                    validator: validateField,
                    controller: tcAddress,
                    readOnly: true,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Street Address',
                    ),
                    onTap: () {
                      if (!viewOnly) {
                        AppState config = AppState.getInstance();
                        showAddressSelectDialog(
                          context: context,
                          searchWidget: MapBoxPlaceSearchWidget(
                            country: config.mapboxSupportedCountries,
                            popOnSelect: true,
                            apiKey: config.mapboxKey,
                            location: Location(
                              lat: config.business.address.coordinate.latitude,
                              lng: config.business.address.coordinate.longitude,
                            ),
                            searchHint: 'Search address',
                            onSelected: (place) {
                              print(place.center);
                              double lng = place.center[0];
                              double lat = place.center[1];
                              String placeName = place.placeName;
                              String postalCode;
                              String city;
                              String province;
                              String country;
                              List<Context> contexts = place.context;
                              for (Context c in contexts) {
                                if (c.id.startsWith('postcode')) {
                                  postalCode = c.text;
                                } else if (c.id.startsWith('place')) {
                                  city = c.text;
                                } else if (c.id.startsWith('region')) {
                                  province = c.text;
                                } else if (c.id.startsWith('country')) {
                                  country = c.text;
                                }
                              }
                              tcAddress.text = placeName;
                              selectedOrder.address.streetAddress = placeName;
                              selectedOrder.address.city =  city;
                              selectedOrder.address.province = province;
                              selectedOrder.address.country = country;
                              selectedOrder.address.postalCode = postalCode;
                              selectedOrder.address.coordinate = Coordinate(latitude: lat, longitude: lng);
                            },
                            context: context,
                          ),
                        );
                      }
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        width: 100,
                        child: TextFormField(
                          readOnly: viewOnly,
                          controller: tcUnit,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Unit',
                          ),
                        ),
                      ),
                      Flexible(
                        child: TextFormField(
                          style: TextStyle(
                            color: viewOnly ? Colors.blue : null,
                          ),
                          onTap: () {
                            if (viewOnly && tcPhone.text.length > 0) {
                              UiUtil.showCallOrTextAlert(tcPhone.text);
                            }
                          },
                          readOnly: viewOnly,
                          controller: tcPhone,
                          keyboardType: TextInputType.phone,
                          validator: validatePhone,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Phone',
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                  child: TextFormField(
                    readOnly: viewOnly,
                    controller: tcEmail,
                    validator: validateEmail,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Email',
                    ),
                  ),
                ),
                BasicDateTimeField(
                  readOnly: !viewOnly,
                  validator: (field) {
                    if (tcEDT.text.isEmpty) {
                      return "Need Date and Time";
                    }
                    return null;
                  },
                  controller: tcEDT,
                  successCb: (dateTime) {
                    selectedOrder.deliverBy = dateTime;
                  },
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                  child: TextFormField(
                    validator: validateNote,
                    readOnly: viewOnly,
                    maxLines: 3,
                    controller: tcNote,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Note',
                    ),
                  ),
                ),
                Visibility(
                  visible: selectedOrder.key != null && selectedOrder.trackUrl != null,
                  child: OutlineButton(
                    onPressed: () {
                      Clipboard.setData(new ClipboardData(text: selectedOrder.trackUrl));
                    },
                    child: Text('Copy track link'),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Tags: ', textScaleFactor: 1.2),
                          Flexible(
                            child: Text(
                              selectedTags,
                              style: TextStyle(
                                fontStyle: FontStyle.italic,
                              )
                            ),
                          ),
                          Visibility(
                            visible: !viewOnly,
                            child: OutlinedButton(
                              onPressed: () {
                                showOrderTagsDialog(
                                  context: context,
                                  tagSelections: tagSelections,
                                  setParentState: setState,
                                );
                              },
                              child: Text('select'),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Visibility(
                  visible: viewOnly && (selectedOrder.driver != null),
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                    child: selectedOrder == null ? SizedBox() : Row(
                      children: [
                        Text('Driver: ', textScaleFactor: 1.2,),
                        GestureDetector(
                          onTap: () => UiUtil.showCallOrTextAlert(selectedOrder.driver.phoneNumber),
                          child: Text(
                            selectedOrder.driver.toString(),
                            style: TextStyle(color: Colors.blue),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Visibility(
                  visible: !viewOnly,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      SimpleDialogOption(
                        onPressed: () { 
                          if (!UiUtil.isFormReady(_formKey)) {
                            return;
                          }
                          if (selectedOrder.deliverBy.isBefore(DateTime.now())) {
                            UiUtil.showAlert(content: Text('Cannot have past delivery time!'));
                            return;
                          }
                          Navigator.pop(context);
                          selectedOrder.address.unit = tcUnit.text;
                          selectedOrder.email = tcEmail.text;
                          selectedOrder.phoneNumber = tcPhone.text;
                          selectedOrder.note = tcNote.text;
                          selectedOrder.tags.clear();
                          tagSelections.forEach((tag, isSelected) { 
                            if (isSelected) {
                              selectedOrder.tags.add(tag);
                            }
                          });
                          saveOrder(selectedOrder);
                        },
                        child: const Text('Confirm',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      SimpleDialogOption(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: const Text('Cancel'),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        },
      );
    }
  );
  viewOnly = false;
  selectedOrder = null;
}

String validateField(String priceStr) {
  final int maxLength = 200;
  if (priceStr == null || priceStr.isEmpty) {
    return "Cannot be empty";
  }
  if (priceStr.length > maxLength) {
    return "Cannot be over $maxLength characters";
  }
  return null;
}

String validateNote(String note) {
  final int maxLength = 200;
  if (note.length > maxLength) {
    return "Cannot be over $maxLength characters";
  }
  return null;
}

void showOrderTagsDialog({
  @required BuildContext context,
  @required Map<Tag, bool> tagSelections,
  @required Function setParentState,
  Function onChanged,
}) async {
  if (setParentState == null) {
    throw Exception('setParentState cannot be null');
  }
  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (context) {
      return StatefulBuilder(
        builder: (context, setState) {
          List<Widget> tagCheckboxes = [];
          tagSelections.forEach((tag, isSelected) {
            tagCheckboxes.add(CheckboxListTile(
              onChanged: (newValue) {
                setState(() {
                  tagSelections[tag] = newValue;
                });
                if (onChanged != null) {
                  onChanged();
                }
              },
              value: tagSelections[tag],
              title: Text(tag.name),
            ));
          });
          return AlertDialog(
            title: Text("Select tags"),
            content: Container(
              height: 300,
              child: SingleChildScrollView(
                child: Column(
                  children: tagCheckboxes,
                ),
              ),
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  Get.back();
                  setParentState(() {});
                },
                child: Text("Ok"),
              ),
            ],
          );
        },
      );
    },
  );  
}

void showAddressSelectDialog({@required BuildContext context, @required MapBoxPlaceSearchWidget searchWidget}) async {
  await showDialog(
    context: context,
    builder: (BuildContext context) {
      return Scaffold(
        body: Column(
          children: [
            FlatButton(onPressed: () => Navigator.pop(context), child: Text('Cancel')),
            Flexible(child: searchWidget),
          ],
        ),
      );
    }
  );
}

String shortAddress(Address address) {
  var arry = address.streetAddress.split(',');
  return arry[0];
}

MaterialColor getOrderColour(Order order) {
  MaterialColor textColour;
  DateTime promisedTime = order.deliverBy;
  DateTime currentTime = DateTime.now();
  if (promisedTime.isBefore(currentTime)) {
    textColour = Colors.red;
  } else if (currentTime.add(Duration(minutes: Constants.ORDER_WARNING_START)).isAfter(promisedTime)) {
    textColour = Colors.orange;
  } else {
    // textColour = Colors.black as MaterialColor;
  }
  return textColour;
}