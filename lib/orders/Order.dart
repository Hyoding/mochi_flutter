import 'package:mochi_flutter/config/AppState.dart';
import 'package:mochi_flutter/core/model/Address.dart';
import 'package:mochi_flutter/manage/employee/Employee.dart';
import 'package:mochi_flutter/manage/tag/Tag.dart';

class Order {
  DateTime createdDate;
  String key;
  String status;
  DateTime deliverBy; // shd be required
  DateTime deliveredTime;
  Address address; // shd be required
  String note;
  
  // one of below should be required
  String email;
  String phoneNumber;

  List<Tag> tags; // for convenience within the app
  List<String> tagsKey; // for actually communicating to the server
  Employee driver;
  String trackUrl;
  
  Order({
    this.createdDate,
    this.key,
    this.status,
    this.deliverBy,
    this.deliveredTime,
    this.email,
    this.phoneNumber,
    this.note,
    this.address,
    this.tags,
    this.tagsKey,
    this.driver,
    this.trackUrl,
  });

  factory Order.fromJson(Map<String, dynamic> json) {
    List<Tag> tagList = List();
    List tagsKeyList = json['tagsKey'];
    AppState appState = AppState.getInstance();
    appState.tags.forEach((tag) { 
      final idx = tagsKeyList.indexWhere((element) => tag.key == element);
      if (idx >= 0) {
        tagList.add(tag);
      }
    });
    return Order(
      createdDate: json['createdDate'] == null ? null : DateTime.parse(json['createdDate']).toLocal(),
      key: json['key'],
      status: json['status'],
      deliverBy: json['deliverBy'] == null ? null : DateTime.parse(json['deliverBy']).toLocal(),
      deliveredTime: json['deliveredTime'] == null ? null : DateTime.parse(json['deliveredTime']).toLocal(),
      address: Address.fromJson(json['address']),
      email: json['email'],
      phoneNumber: json['phoneNumber'],
      note: json['note'],
      tags: tagList,
      tagsKey: json['tagsKey'].cast<String>(),
      driver: Employee.fromJson(json['driver']),
      trackUrl: json['trackUrl'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'createdDate': createdDate == null ? null : createdDate.toUtc().toIso8601String(),
      'key': key,
      'status': status,
      'deliverBy': deliverBy == null ? null : deliverBy.toUtc().toIso8601String(),
      'deliveredTime': deliveredTime == null ? null : deliveredTime.toUtc().toIso8601String(),
      'address': address.toJson(),
      'email': email,
      'phoneNumber': phoneNumber,
      'note': note.trim(),
      'tagsKey': tags.map((t) => t.key).toList(),
    };
  }

}