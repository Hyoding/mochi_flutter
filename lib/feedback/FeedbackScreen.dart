import 'package:flutter/material.dart';
import 'package:mochi_flutter/config/Constants.dart';
import 'package:mochi_flutter/core/MapBuilder.dart';
import 'package:mochi_flutter/util/HttpUtil.dart';

import '../core/components/BaseDrawerScreen.dart';

class FeedbackScreen extends BaseDrawerScreen {
  static const String PATH = '/feedback';
  static const String TITLE = 'Feedback';
  @override
  Widget getBody(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: FeedbackWidget(),
    );
  }

  @override
  String getTitle() {
    return TITLE;
  }

}

class FeedbackWidget extends StatefulWidget {
  @override
  _FeedbackWidgetState createState() => _FeedbackWidgetState();
}

class _FeedbackWidgetState extends State<FeedbackWidget> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController tcFeedback = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            TextFormField(
              maxLines: 10,
              validator: validateFeedback,
              controller: tcFeedback,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Feedback',
              ),
            ),
            Container(
              width: double.infinity,
              child: OutlinedButton(
                onPressed: submitFeedback,
                child: Text('SUBMIT'),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void submitFeedback() {
    if (!_formKey.currentState.validate()) {
      return;
    }
    
    MapBuilder mb = HttpUtil.createMap();
    mb.addEntry('userType', 'EMPLOYEE');
    mb.addEntry('message', tcFeedback.text.trim());
    HttpUtil.postConveniently(
      url: Url.SUBMIT_FEEDBACK,
      data: mb.map,
      cb: (_) {
        setState(() {
          tcFeedback.text = '';
        });
      },
    );
  }

  String validateFeedback(String priceStr) {
    final int maxLength = 200;
    if (priceStr == null || priceStr.isEmpty) {
      return "Cannot be empty";
    }
    if (priceStr.length > maxLength) {
      return "Cannot be over $maxLength characters";
    }
    return null;
  }
}