class Constants {
  static const String APP_NAME = 'Mochi';
  static const String STORAGE_USER_EMAIL = 'userEmail';
  static const int ORDER_WARNING_START = 30;
}

class Url {

  static const String BASE = 'http://192.168.0.103:61611/api';
  // static const String BASE = 'http://68.183.127.26:61611/api';
  static const String PING = '$BASE/ping';

  // business
  static const String REGISTER = '$BASE/business/v1/register';
  static const String GET_MOBILE_APP_CONFIG = '$BASE/business/v1/getMobileAppConfig';
  static const String VALIDATE_LOGIN = '$BASE/business/v1/validateLogin';
  static const String GET_ORDERS_TO_DELIVER = '$BASE/business/v1/getOrdersToDeliver';
  static const String GET_ACTIVE_ORDERS = '$BASE/business/v1/getActiveOrders';
  static const String CREATE_ORDER = '$BASE/business/v1/createOrder';
  static const String UPDATE_ORDER = '$BASE/business/v1/updateOrder';
  static const String REJECT_ORDER = '$BASE/business/v1/rejectOrder';
  static const String GET_ORDER_HISTORY = '$BASE/business/v1/getOrderHistory';
  static const String GET_ACTIVE_SUMMARY = '$BASE/business/v1/getActiveOrdersSummary';

  // Trip
  static const String START_TRIP = '$BASE/trip/v1/startTrip';
  static const String CURRENT_TRIP_EXISTS = '$BASE/trip/v1/currentTripExists';
  static const String FIND_CURRENT_TRIP = '$BASE/trip/v1/findCurrentTrip';
  static const String DELIVERED_ORDER = '$BASE/trip/v1/deliveredOrder';
  static const String FINISH_TRIP = '$BASE/trip/v1/finishTrip';
  static const String UPDATE_TRIP_LOCATION = '$BASE/trip/v1/updateLocation';
  static const String UPDATE_TRIP_SEQUENCE = '$BASE/trip/v1/updateOrderSequence';

  // Feedback
  static const String SUBMIT_FEEDBACK = '$BASE/feedback/v1/employeeSubmit';

  // Feset password
  static const String RESET_PWD = '$BASE/resetPassword/v1/employee';

  // manage
  static const String ADD_EMPLOYEE = '$BASE/business/v1/addEmployee';
  static const String REMOVE_EMPLOYEE = '$BASE/business/v1/removeEmployee';
  static const String UPDATE_EMPLOYEE = '$BASE/business/v1/updateEmployee';
  static const String GET_EMPLOYEES = '$BASE/business/v1/getEmployees';
  static const String ADD_TAG = '$BASE/business/v1/addTag';
  static const String GET_TAGS = '$BASE/business/v1/getTags';
  static const String REMOVE_TAG = '$BASE/business/v1/removeTag';
  static const String UPDATE_TAG = '$BASE/business/v1/updateTag';
  static const String GET_SUBSCRIPTION_PLANS = '$BASE/business/v1/getSubscriptionPlans';
  static const String CHANGE_SUBSCRIPTION_PLAN = '$BASE/business/v1/changeSubscriptionPlan';

  // report
  static const String REQUEST_REPORT = '$BASE/report/v1/emailReport';
  
}