import 'package:mochi_flutter/core/model/Business.dart';
import 'package:mochi_flutter/manage/employee/Employee.dart';
import 'package:mochi_flutter/manage/tag/Tag.dart';

class AppState {
  static AppState _instance;
  AppState._();
  static AppState getInstance() {
    if (_instance == null) {
      _instance = AppState._();
    }
    return _instance;
  }
  static void setInstance(AppState appState) {
    _instance = appState;
  }

  Business business;
  Employee employee;
  String mapboxKey;
  String mapboxStyle;
  String mapboxSupportedCountries;
  int updateLocationFrequency;
  double updateLocationDistance;
  int tripMaxNumbOrders;
  List<Tag> tags;
  int defaultETA;
  String urlWebImportOrders;
  String urlWeb;

  AppState({this.business, this.employee, this.mapboxKey, this.updateLocationFrequency, this.mapboxStyle, this.tripMaxNumbOrders, this.tags, this.mapboxSupportedCountries, this.defaultETA, this.updateLocationDistance, this.urlWebImportOrders, this.urlWeb});
  
  factory AppState.fromJson(Map<String, dynamic> json) {
    List tagsList = json['tags'];
    return AppState(
      business: Business.fromJson(json['business']),
      employee: Employee.fromJson(json['employee']),
      mapboxKey: json['mapboxKey'],
      mapboxStyle: json['mapboxStyle'],
      updateLocationFrequency: json['updateLocationFrequency'],
      updateLocationDistance: json['updateLocationDistance'],
      tripMaxNumbOrders: json['tripMaxNumbOrders'],
      tags: tagsList.map((t) => Tag.fromJson(t)).toList(),
      mapboxSupportedCountries: json['mapboxSupportedCountries'],
      defaultETA: json['defaultETA'],
      urlWebImportOrders: json['urlWebImportOrders'],
      urlWeb: json['urlWeb'],
    );
  }
}