import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mochi_flutter/login/ForgotPasswordScreen.dart';
import 'package:mochi_flutter/login/LoginScreen.dart';
import 'package:mochi_flutter/login/NoConnectionScreen.dart';
import 'package:mochi_flutter/manage/ManageScreen.dart';
import 'package:mochi_flutter/manage/employee/ManageEmployeeScreen.dart';
import 'package:mochi_flutter/manage/report/GenerateReportScreen.dart';
import 'package:mochi_flutter/manage/tag/ManageTagScreen.dart';
import 'package:mochi_flutter/orders/OrdersScreen.dart';
import 'package:mochi_flutter/feedback/FeedbackScreen.dart';
import 'package:mochi_flutter/trips/MyTripsScreen.dart';
import 'package:mochi_flutter/settings/SettingsScreen.dart';
import 'login/AppConfigLoader.dart';
import 'login/RegisterScreen.dart';
import 'manage/employee/AddEditEmployeeScreen.dart';
import 'manage/tag/AddEditTagScreen.dart';
import 'package:location_permissions/location_permissions.dart';

String initialScreen;
bool noConnection = false;
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await LocationPermissions().requestPermissions();
  initialScreen = await AppConfigLoader.initScreen();
  runApp(GetMaterialApp(
    home: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: initialScreen,
      routes: {
        LoginScreen.PATH: (context) => LoginScreen(),
        ManageScreen.PATH: (context) => ManageScreen(),
        OrdersScreen.PATH: (context) => OrdersScreen(),
        MyTripScreen.PATH: (context) => MyTripScreen(),
        SettingsScreen.PATH: (context) => SettingsScreen(),
        FeedbackScreen.PATH: (context) => FeedbackScreen(),
        ForgotPasswordScreen.PATH: (context) => ForgotPasswordScreen(),
        RegisterScreen.PATH: (context) => RegisterScreen(),
        NoConnectionScreen.PATH: (context) => NoConnectionScreen(),
        ManageEmployeeScreen.PATH: (_) => ManageEmployeeScreen(),
        ManageTagScreen.PATH: (_) => ManageTagScreen(),
        GenerateReportScreen.PATH: (_) => GenerateReportScreen(),        
      },
      onGenerateRoute: (settings) {
        final arguments = settings.arguments as Map<String, dynamic>;
        Widget widget;
        switch (settings.name) {
          case AddEditEmployeeScreen.PATH:
            widget = AddEditEmployeeScreen(
              viewOnly: arguments['viewOnly'],
              employee: arguments['employee'],
            );
            break;
          case AddEditTagScreen.PATH:
            widget = AddEditTagScreen(
              viewOnly: arguments['viewOnly'],
              tag: arguments['tag'],
            );
            break;
          default:
            return null;
        }
        return MaterialPageRoute(
          builder: (_) {
            return widget;
          },
        );
      },
    );
  }
}
