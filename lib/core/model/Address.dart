import 'Coordinate.dart';

class Address {
  String streetAddress;
  String unit;
	String city;
	String province;
	String country;
	String postalCode;
  Coordinate coordinate;

  Address({this. streetAddress, this.unit, this.city, this.province, this.country, this.postalCode, this.coordinate});

  factory Address.fromJson(Map<String, dynamic> json) {
    return Address(
      streetAddress: json['streetAddress'],
      unit: json['unit'],
      city: json['city'],
      province: json['province'],
      country: json['country'],
      postalCode: json['postalCode'],
      coordinate: Coordinate.fromJson(json['coordinate']),
    );
  }

  Map<String, dynamic> toJson() => {
      'streetAddress': streetAddress,
      'unit': unit?.trim(),
      'city': city?.trim(),
      'province': province?.trim(),
      'country': country?.trim(),
      'postalCode': postalCode?.trim(),
      'coordinate': coordinate?.toJson(),
  };

}