import 'Address.dart';

class Business {
  String key;
	String name;
	String phoneNumber;
	Address address;
  
  Business({this.key, this.name, this.phoneNumber, this.address});

  factory Business.fromJson(Map<String, dynamic> json) {
    return Business(
      key: json['key'],
      name: json['name'],
      phoneNumber: json['phoneNumber'],
      address: Address.fromJson(json['address']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'key': key,
      'name': name,
      'phoneNumber': phoneNumber,
      'address': address.toJson(),
    };
  }
  
}