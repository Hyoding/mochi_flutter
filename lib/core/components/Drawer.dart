import 'package:flutter/material.dart';
import 'package:mochi_flutter/config/AppState.dart';
import 'package:mochi_flutter/config/Constants.dart';
import 'package:mochi_flutter/manage/employee/Employee.dart';
import 'package:mochi_flutter/manage/ManageScreen.dart';
import 'package:mochi_flutter/orders/OrdersScreen.dart';
import 'package:mochi_flutter/feedback/FeedbackScreen.dart';
import 'package:mochi_flutter/settings/SettingsScreen.dart';
import 'package:mochi_flutter/trips/MyTripsScreen.dart';

class MyDrawer extends StatelessWidget {

  String getCurrentRoute(BuildContext context) {
    var route = ModalRoute.of(context);
    if(route != null){
        return route.settings.name;
    }
    return '';
  }

  bool isSameRoute(BuildContext context, String requestedRoute) {
    String currentRoute = getCurrentRoute(context);
    return currentRoute == requestedRoute;
  }

  @override
  Widget build(BuildContext context) {
    Employee employee = AppState.getInstance().employee;
    String type = employee.type;
    return Drawer(
      // Add a ListView to the drawer. This ensures the user can scroll
      // through the options in the drawer if there isn't enough vertical
      // space to fit everything.
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // Image.asset(
                //   'assets/icon/mochi.svg',
                //   height: 60,
                // ),
                Text(
                  Constants.APP_NAME,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 30,
                  ),
                ),
              ],
            ),
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
            ),
          ),
          Visibility(
            visible: type == 'Owner' || type == 'Manager',
            child: ListTile(
              title: Text(ManageScreen.TITLE),
              onTap: () {
                if (!isSameRoute(context, ManageScreen.PATH)) {
                  Navigator.pushNamedAndRemoveUntil(context, ManageScreen.PATH, (r) => false);
                } else {
                  Navigator.pop(context);
                }
              },
            ),
          ),
          Visibility(
            visible: type == 'Owner' || type == 'Manager' || type == 'Operator',
            child: ListTile(
              title: Text(OrdersScreen.TITLE),
              onTap: () {
                if (!isSameRoute(context, OrdersScreen.PATH)) {
                  Navigator.pushNamedAndRemoveUntil(context, OrdersScreen.PATH, (r) => false);
                } else {
                  Navigator.pop(context);
                }
              },
            ),
          ),
          Visibility(
            visible: type == 'Owner' || type == 'Manager' || type == 'Driver',
            child: ListTile(
              title: Text(MyTripScreen.TITLE),
              onTap: () {
                Navigator.pushNamedAndRemoveUntil(context, MyTripScreen.PATH, (r) => false);            
              },
            ),
          ),
          ListTile(
            title: Text(FeedbackScreen.TITLE),
            onTap: () {
              if (!isSameRoute(context, FeedbackScreen.PATH)) {
                Navigator.pushNamedAndRemoveUntil(context, FeedbackScreen.PATH, (r) => false);
              } else {
                Navigator.pop(context);
              }
            },
          ),
          ListTile(
            title: Text(SettingsScreen.TITLE),
            onTap: () {
              if (!isSameRoute(context, SettingsScreen.PATH)) {
                Navigator.pushNamedAndRemoveUntil(context, SettingsScreen.PATH, (r) => false);
              } else {
                Navigator.pop(context);
              }
            },
          ),
        ],
      ),
    );
  }
}
