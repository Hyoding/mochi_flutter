import 'package:flutter/material.dart';
import 'package:mochi_flutter/core/components/Drawer.dart';

abstract class BaseDrawerScreen extends StatelessWidget {
  String getTitle();
  Widget getBody(BuildContext context);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(getTitle()),
        centerTitle: true,
      ),
      body: getBody(context),
      drawer: MyDrawer(),
    );
  }
}
