class MapBuilder<K,V> {
  Map<K,V> map = Map();

  MapBuilder<K,V> addEntry(K key, V value) {
    map.putIfAbsent(key, () => value);
    return this;
  }

  MapBuilder<K,V> merge(Map source) {
    source.forEach((key, value) { addEntry(key, value); });
    return this;
  }

}