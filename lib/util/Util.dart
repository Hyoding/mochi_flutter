import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:mochi_flutter/config/Constants.dart';
import 'package:mochi_flutter/core/model/Coordinate.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_sms/flutter_sms.dart';

class Util {  
  
  Util._();

  static DateFormat dateFormat = DateFormat("yyyy-MM-dd hh:mm a");
  static String formatDate(DateTime dateTime) {
    return dateFormat.format(dateTime);
  }

  static DateFormat shortDateFormat = DateFormat("hh:mm a");
  static String formatShortDate(DateTime dateTime) {
    return shortDateFormat.format(dateTime);
  }

  static Future<Coordinate> getMyCoordinate() async {
    Location location = new Location();
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;
   
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return null;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return null;
      }
    }

    _locationData = await location.getLocation();
    Coordinate coord = Coordinate(latitude: _locationData.latitude, longitude: _locationData.longitude);
    return coord;
  }

  static bool isEmail(String email) {
    return GetUtils.isEmail(email);
  }

  static bool isPhoneNumber(String phoneNumber) {
    return GetUtils.isPhoneNumber(phoneNumber);
  }

  static Future<bool> isLoggedIn() async {
    final prefs = await SharedPreferences.getInstance();
    String email;
    try {
      email = prefs.getString(Constants.STORAGE_USER_EMAIL);
    } catch (ex) {
      return false;
    }
    if (email.isNullOrBlank) {
      return false;
    }
    return true;
  }

  static Future<dynamic> getLocalStorage(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.get(key);
  }

  static Future<void> removeLoginStorage() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(Constants.STORAGE_USER_EMAIL);
  }

  static Future<dynamic> getMyEmail() async {
    return getLocalStorage(Constants.STORAGE_USER_EMAIL);
  }

  static void sendSms(String recipient) async {
    List<String> recipients = List();
    recipients.add(recipient);
    String _result = await sendSMS(message: '', recipients: recipients)
        .catchError((onError) {
          print(onError);
        });
    print(_result);
  }

  static void call(String number) {
    launch('tel://$number');
  }

}
