import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mochi_flutter/config/AppState.dart';
import 'package:mochi_flutter/core/MapBuilder.dart';
import 'package:mochi_flutter/core/model/MyHttpResponse.dart';
import 'package:mochi_flutter/util/UiUtil.dart';

class HttpUtil {

  static final Dio _dio = Dio();

  HttpUtil._();

  static Future<Response<dynamic>> get({
    @required String url,
    Map<String, dynamic> queryParameters,
  }) async {
    return _dio.get(url, queryParameters: queryParameters);
  }
  
  static Future<Response<dynamic>> post({
    @required String url,
    Map<String, dynamic> data,
  }) async {
    return _dio.post(url, data: data);
  }

  static Future getConveniently({
    @required String url,
    @required Function(MyHttpResponse) cb,
    Map<String, dynamic> queryParameters,
    bool showErrMsg = true,
  }) async {
    try {
      final response = await get(url: url, queryParameters: queryParameters);
      MyHttpResponse myResponse = MyHttpResponse.fromJson(response.data);
      if (response.statusCode == 200) {
        if (cb != null) {
          cb.call(myResponse);
        }
      }
    } on DioError catch (ex) {
      if (showErrMsg) {
        showHttpErrorDialog(ex);
      }
      throw ex;
    }
  }

  static Future postConveniently({
    @required String url,
    Function(MyHttpResponse) cb,
    Map<String, dynamic> data,
    bool showAlertOnSuccess = true,
    bool showAlertOnFail = true,
    bool showProgressIndicator = true,
  }) async {
    if (data == null) {
      data = Map();
    }
    AppState appState = AppState.getInstance();
    data.addEntries([
      MapEntry('requester', {
        'userKey': appState.employee == null ? null : appState.employee.key,
        'from': 'mobile',
      }),
      MapEntry('businessKey', appState.business == null ? null : appState.business.key),
    ]);
    try {
      if (showProgressIndicator) {
        UiUtil.showAlert(
          title: '',
          content: CircularProgressIndicator(),
          barrierDismissible: false,
        );
      }
      final response = await post(url: url, data: data);
      if (showProgressIndicator) {
        Get.back();
      }
      MyHttpResponse myResponse = MyHttpResponse.fromJson(response.data);
      if (response.statusCode == 200) {
        if (showAlertOnSuccess) {
          String msg = myResponse.message == null ? "" : myResponse.message;
          UiUtil.showAlert(
            title: 'Success',
            content: Text(msg),
            barrierDismissible: cb == null ? true : false,
            onConfirm: cb == null ? null : () {
              cb.call(myResponse);
            }
          );
        }
      }
    } on DioError catch (ex) {
      if (showProgressIndicator) {
        Get.back();
      }
      if (showAlertOnFail) {
        showHttpErrorDialog(ex);
      }
      throw ex;
    }
  }

  static void showHttpErrorDialog(dynamic ex) {
    String msg = '';
    if (ex.response != null) {
      msg = ex.response.data['message'];
    }
    if (msg.isEmpty) {
      msg = 'Something went wrong, if problem continues, please contact the developer';
    }
    UiUtil.showAlert(
      title: 'Oops',
      content: Text(msg),
    );
  }

  static Map<String, dynamic> formParams(String key, dynamic value) {
    Map<String, dynamic> map = Map();
    map.addEntries([
      MapEntry(key, value)
    ]);
    return map;
  }

  static MapBuilder<String, dynamic> createMap() {
    return MapBuilder();
  }

}