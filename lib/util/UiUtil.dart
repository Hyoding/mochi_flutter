import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'Util.dart';

class UiUtil {
  UiUtil._();

  static void showAlert({
    @required Widget content,
    String title = 'Alert',
    VoidCallback onConfirm,
    VoidCallback onCancel,
    bool barrierDismissible = true,
  }) {
    Get.defaultDialog(
      barrierDismissible: barrierDismissible,
      titleStyle: TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: 16,
      ),
      title: title,
      radius: 10,
      content: content,
      buttonColor: Colors.white,
      onConfirm: onConfirm == null ? null : () {
        Get.back();
        onConfirm();
      },
      onCancel: onCancel == null ? null : () {
        Get.back();
        onCancel();
      },
    );
  }

  static bool isFormReady(GlobalKey<FormState> formKey) {
    if (!formKey.currentState.validate()) {
      return false;
    }
    return true;
  }

  static void showCallOrTextAlert(String phoneNumber) {
    UiUtil.showAlert(
      title: 'Contact',
      content: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          OutlinedButton(
            onPressed: () {
              Util.sendSms(phoneNumber);
              Get.back();
            },
            child: Text('Text'),
          ),
          OutlinedButton(
            onPressed: () {
              Util.call(phoneNumber);
              Get.back();
            },
            child: Text('Call'),
          ),
        ],
      ),
    );
  }

}